<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">

</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, shrink-to-fit=no, initial-scale=1">


<meta name="description" content="">
<meta name="author" content="">

<title>Profile de ${user_view.getNomUser()} ${user_view.getPrenomUser()}</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Exo"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/stylesheet.css" rel="stylesheet">
<link href="resources/css/bootstrap-datepicker3.min.css"
	rel="stylesheet">
<link href="resources/css/profile1.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
	<div id="wrapper" class="toggled">

		<%@ include file="../vues/fragment/main_nav.jspf"%>
		<!-- Sidebar -->
		<%@ include file="../vues/fragment/side_nav.jspf"%>

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<button type="button" class="hamburger is-closed"
				style="margin-top: 50px;" data-toggle="offcanvas">
				<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
					class="hamb-bottom"></span>
			</button>
			<div class="container main col-md-11">
				<div class="col-md-12 fixed">
					<%@ include file="../vues/fragment/rechercheTrajet.jspf"%>
				</div>

				<div class="row well p_form col-md-11 ">
					<div class="row profile">
						<div class="col-md-4">
							<div class="profile-sidebar">
								<!-- SIDEBAR USERPIC -->
								<div class="profile-userpic">
									<img class="" src="resources/img/iconProfile.png" alt="Profile"
												style="display:block; width: 60%; height: auto; margin-right: auto;, margin-left: auto;">
								</div>
								<!-- END SIDEBAR USERPIC -->
								<!-- SIDEBAR USER TITLE -->
								<div class="profile-usertitle">
									<div class="profile-usertitle-name">${user_view.getNomUser()} ${user_view.getPrenomUser()}</div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
								<!-- SIDEBAR BUTTONS -->
								<div class="profile-userbuttons">
								<a href="#mess">
									<button type="button" class="btn btn-primary btn-lg"
										style="font-size:20px;">Message</button></a>
								</div>
								<!-- END SIDEBAR BUTTONS -->
								<!-- SIDEBAR MENU -->
								<div class="profile-usermenu">
									${user_view.getEmailUtilisateur()} </br> 22ans </br>
									${user_view.getAdresseUtilisateur()}
								</div>
								<!-- END MENU -->
							</div>
						</div>
						<div class="profile-cont col-md-8">
								<div class="row row1 well col-md-12">
									<div class="panel-group" id="accordion">


										<c:forEach var="commande" items="${user_commandes}" varStatus="loop">
											<div class="panel panel-default">
												<div class="panel-heading ">
													<h4 class="row row1">
														<div class="col-md-3 hed ">
															<div class="wel ">Depart :</div>
															${commande.getPositionColis()}
														</div>
														<div class="col-md-3 hed">
															<div class="wel">Destination :</div>
															${commande.getDestinationColis()}
														</div>
														<div class="col-md-3 hed">
															<div class="wel">Date :</div>
															
														</div>

														<div class="col-md-3 det">
															<a data-toggle="collapse" data-parent="#accordion"
																href="#collapse${loop.index }"> Details <span
																class="glyphicon glyphicon-list-alt"></span>
															</a>
														</div>
													</h4>
												</div>
												<div id="collapse${loop.index }" class="panel-collapse collapse">
													<div class="panel-body">Lorem ipsum dolor sit amet,
														consectetur adipisicing elit, sed do eiusmod tempor
														incididunt ut labore et dolore magna aliqua. Ut enim ad
														minim veniam, quis nostrud exercitation ullamco laboris
														nisi ut aliquip ex ea commodo consequat.</div>
												</div>
											</div>
										</c:forEach>



									</div>
								</div>
								
								<div class="row row1 well col-md-12" id="mess">

									<div class=" well" id="cont"
										style="text-align: center; font-size: 22px;">Envoyer un
										message</div>

									
										<form:form modelAttribute="MessageForm" id="msgForm"
											method="GET" action="EnvoyerMessagePerso">
											
										<fieldset>
											<div class="form-group">
												<div class="col-md-12 inputGroupContainer">
													<div class="input-group">
														<span class="input-group-addon"><i
															class="glyphicon glyphicon-list"></i></span>
															<form:textarea  placeholder="Votre message"
													class="form-control" path="text" />
													
													</div>
												</div>
											</div>

											<form:input type="hidden" value="${user_view.getIdUser()}"
													class="form-control" path="idUser" />
											
											<form:input type="hidden" value="-1"
													class="form-control" path="idDemmande" />
											<!-- Success message -->
											<div class="alert alert-success" role="alert"
												id="success_message">
												Success <i class="glyphicon glyphicon-thumbs-up"></i> Thanks
												for contacting us, we will get back to you shortly.
											</div>

											<!-- Button -->
											<div class="form-group">
												<label class="col-md-4 control-label"></label>
												<div class="col-md-6">
													<button type="submit" class="btn btn-success path_form_val">Envoyer</button>
												</div>
											</div>
										</fieldset>
									</form:form>
								</div>
							
						</div>
					</div>



				</div>




				<!-- /#page-content-wrapper -->

			</div>
			<!-- /#wrapper -->
		</div>
	</div>

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.fr.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Menu Toggle Script -->
	<script src="resources/js/side.js"></script>
	<script>
		$('#datep').datepicker({});
	</script>
	<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaIa3iiMjYNRq2e6NVQNKF4KU3v7pi77I&libraries=places&callback=initialize">
		
	</script>

	<script src="resources/js/autocomplete.js"></script>
</body>

</html>
