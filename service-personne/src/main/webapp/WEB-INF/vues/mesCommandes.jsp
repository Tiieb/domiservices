<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">
</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, shrink-to-fit=no, initial-scale=1">


<meta name="description" content="">
<meta name="author" content="">

<title>Tableau de bord</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Exo"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/bootstrap-datepicker3.min.css"
	rel="stylesheet">
<link href="resources/css/stylesheet.css" rel="stylesheet">



</head>

<body>


	<div id="wrapper" class="toggled">
		<%@ include file="../vues/fragment/main_nav.jspf"%>
		<!-- Sidebar -->
		<%@ include file="../vues/fragment/side_nav.jspf"%>

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<button type="button" class="hamburger is-closed"
				style="margin-top: 50px;" data-toggle="offcanvas">
				<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
					class="hamb-bottom"></span>
			</button>
			<div class="container main col-md-10"">
				<div class="col-md-12 fixed">
					<!-- rechercher-->
					<%@ include file="../vues/fragment/rechercheTrajet.jspf"%>
				</div>

				<!-- 				<div class="row well p_form col-md-12" -->
				<!-- 					style="text-align: center; font-size: 22px;">Mes Commandes</div> -->

				<div class="row jumbotron col-md-12 p_form" style="padding: 20px;">

					<c:if test="${not empty CommandeAjouté}">
						<div class="alert alert-success">${CommandeAjouté}</div>
					</c:if>
					<c:if test="${not empty listeVide}">
						<div class="alert alert-danger">${listeVide}</div>
					</c:if>
					<c:if test="${not empty messageInfo}">
						<div class="alert alert-success">${messageInfo}</div>
					</c:if>
					<c:if test="${not empty listeDesCommandes}">
						<div class="panel-group" id="accordion">
							<c:forEach var="commande" items="${listeDesCommandes}"
								varStatus="loop">


								<div class="panel panel-default">

									<c:forEach var="demmande"
										items="${listesDesDemmande.get(loop.index)}" varStatus="loop1">

										<c:if test="${demmande.getEtat().equals('En cours')}">
											<c:set var="test" value='t' scope="page" />
										</c:if>

									</c:forEach>
									<c:if test="${test=='t'}">
										<div class="panel-heading " style="background-color: #aef9b3;">
									</c:if>
									<c:if test="${test=''}">
										<div class="panel-heading ">
									</c:if>

									<div class="panel-heading ">
										<h4 class="row" style="margin-top: 0px;">
											<div class="col-md-2 first_hed">
												<img class="" src="resources/img/iconProfile.png"
													alt="Profile"
													style="float: left; width: 40px; height: 40px; margin-right: 5px;">
												${ sessionScope.utilisateurSession.nomUser} </br>${ sessionScope.utilisateurSession.prenomUser}
											</div>
											<div class="col-md-8">
												<div class="col-md-4 hed ">
													<div class="wel ">Depart :</div>
													${commande.positionColis}
												</div>
												<div class="col-md-4 hed">
													<div class="wel">Destination :</div>
													${commande.destinationColis}
												</div>
												<div class="col-md-4 hed">
													<div class="wel">Date :</div>
													<%-- ${commande.dateDepart} --%>
												</div>
											</div>
											<div class="col-md-2 det">
												<div class="col-md-6 hed ">
													<a
														href="modifierCommande?idCommande=${commande.idCommande}">
														<input type="button" value="Modifier"
														class="btn btn-primary"
														style="width: 60px; font-size: 10px; padding: 2px;">
													</a>
												</div>
												<div class="col-md-6 hed ">
													<a
														href="supprimerCommande?idCommande=${commande.idCommande}">
														<input type="button" value="Supprimer"
														class="btn btn-danger"
														style="width: 60px; font-size: 10px; padding: 2px;">
													</a>
												</div>
											</div>
										</h4>
										<hr id="line">
										<div class="row" style="text-align: center;">
											<a data-toggle="collapse" data-parent="#accordion"
												href="#collapse${loop.index}"> Plus <span
												class="glyphicon glyphicon-plus"></span>
											</a>
										</div>
									</div>
									<div id="collapse${loop.index}" class="panel-collapse collapse">
										<div class="panel-body">

											<c:if test="${not empty demmandeVide}">
												<div class="alert alert-danger">${demmandeVide}</div>

											</c:if>

											<c:if test="${empty listesDesDemmande.get(loop.index)}">
												<div class="alert alert-danger">Vous n'avez aucune
													demmande pour cette commnde</div>

											</c:if>

											<c:if test="${not empty listesDesDemmande}">
												<c:forEach var="demmande"
													items="${listesDesDemmande.get(loop.index)}"
													varStatus="loop1">

													<c:choose>
														<c:when test="${demmande.getEtat().equals('En cours')}">
															<div class="panel panel-default "
																style="background-color: #99bdf7;">
																<div class="panel-heading "
																	style="background-color: #99bdf7;">
														</c:when>
														<c:otherwise>
															<div class="panel panel-default">
																<div class="panel-heading">
														</c:otherwise>
													</c:choose>

													<h4 class="row">
														<div class="col-md-3 first_hed">
															<img class="" src="resources/img/iconProfile.png"
																alt="Profile"
																style="float: left; width: 40px; height: 40px; margin-right: 5px;">
															${listeDesUtilisateurs.get(loop.index).get(loop1.index).getNomUser()}
															</br>
															${listeDesUtilisateurs.get(loop.index).get(loop1.index).getPrenomUser()}
															<%-- 																${Users[loop.index].getPrenomUser()} --%>
														</div>
														<div class="col-md-6 ">
															<div class="col-md-6 hed ">${demmande.getEtat()}</div>
															<div class="col-md-6 hed">${demmande.getMdp()}</div>

														</div>
														<div class="col-md-3 det">
															<c:if test="${test=='t'}">

															</c:if>
															<c:if test="${test==''}">
																<a
																	href="AccepterDemmande?idDemmande=${demmande.getIdDemmande()}">
																	<input type="button" value="Accepter"
																	class="btn btn-primary">
																</a>
															</c:if>
															<script language="javascript">
													</script>
															<!-- 															<a -->
															<%-- 																href="EnvoyerMessage?idDemmande=${demmande.getIdDemmande()}"> --%>
															<input type="button" value="Message"
																class="btn btn-success" style="margin-right: 10px;"
																onclick="toggle(${demmande.getIdDemmande()});">
															<!-- 															</a> -->
														</div>
													</h4>

													<script src="resources/js/message.js"></script>
										</div>
									</div>
									<div class="msg" id="msg${demmande.getIdDemmande()}"
										style="text-align: center; height: 0px; visibility: hidden;">

										<form:form modelAttribute="MessageForm" id="msgForm"
											method="GET" action="EnvoyerMessage">
											<fieldset>
												<!-- 												<textarea path="text" rows="4" cols="100" name="message" -->
												<!-- 													form="msgForm" placeholder="Ecrviez votre message ici"> </textarea> -->

												<form:input type="text"
													placeholder="Votre ville de depart..." class="form-control"
													path="text" />

												<form:input type="hidden"
													value="${demmande.getIdDemmande()}" class="form-control"
													path="idDemmande" />

												<form:input type="hidden" value="${demmande.getIdUser()}"
													class="form-control" path="idUser" />

												</br>
												<button class="btn btn-default" type="submit"
													value="Envoyer" />
											</fieldset>
										</form:form>
									</div>
									<c:set var="test" value='' scope="page" />
							</c:forEach>
					</c:if>
				</div>
			</div>
		</div>
	</div>
	</c:forEach>
	</c:if>
	</div>

	</div>
	</div>
	<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.fr.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Menu Toggle Script -->
	<script src="resources/js/side.js"></script>

</body>

</html>


