<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">
</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, shrink-to-fit=no, initial-scale=1">


<meta name="description" content="">
<meta name="author" content="">

<title>Tableau de bord</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Exo"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/bootstrap-datepicker3.min.css"
	rel="stylesheet">
<link href="resources/css/stylesheet.css" rel="stylesheet">
<link href="resources/css/stylesheet1.css" rel="stylesheet">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>


	<div id="wrapper" class="toggled">

		<%@ include file="../vues/fragment/main_nav.jspf"%>
		<!-- Sidebar -->
		<%@ include file="../vues/fragment/side_nav.jspf"%>

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<button type="button" class="hamburger is-closed"
				style="margin-top: 50px;" data-toggle="offcanvas">
				<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
					class="hamb-bottom"></span>
			</button>
			<div class="container main col-md-10">
				<div class="col-md-12 fixed">
					<%@ include file="../vues/fragment/rechercheTrajet.jspf"%>
				</div>

				<div class="row jumbotron col-md-9 p_form">
					<c:if test="${not empty CommandeAjouter}">
						<div class="alert alert-success">${CommandeAjouter}</div>
					</c:if>

					<c:if test="${not empty ErreurAjoutCommande}">
						<div class="alert alert-danger">${ErreurAjoutCommande}</div>
					</c:if>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.fr.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Menu Toggle Script -->
	<script src="resources/js/side.js"></script>


</body>

</html>


