<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">
	<c:redirect url="espacePersoDisplay" />
</c:if>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>Domi Services</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet" type="text/css" />
<!-- Theme CSS -->
<link href="resources/css/freelancer.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/form-style.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/form-elements.css" rel="stylesheet"
	type="text/css" />

<!-- Custom Fonts -->
<link href="resources/vendor/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700"
	rel="stylesheet" type="text/css" />
<link
	href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic"
	rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

	<%@ include file="../vues/fragment/head_nav.jspf"%>

	<!-- Header -->
	<header>
		<div id="maincontent" tabindex="-1">
			<div class="success" id="connexion">
				<div class="col-md-12">

					<div class="form-box" id="connexion-form">
						<div class="connexion">
							<c:if test="${not empty messageInfo}">
								<div class="alert alert-success">${messageInfo}</div>
							</c:if>
							<c:if test="${not empty messageErreur}">
								<div class="alert alert-danger">${messageErreur}</div>
							</c:if>
							<%@ include file="../vues/fragment/connexion.jspf"%>
						</div>
					</div>

				</div>
			</div>
			<div class="row main-cont" style="margin-top: 100px">
				<div class="col-md-8">
					<img class="img-responsive" src="../resources/img/groceries.png"
						alt="" style="margin-top: 30px">
					<div class="intro-text">
						<h1 class="name" style="margin-bottom: 30px;">Domi Services</h1>

						<span class="skills">Application d'aide a la personne</span></br> <span
							class="skills">Courses & Livraisons</span>
					</div>

				</div>
				<div class="col-md-4">
					<div class="col-sm-12">

						<div class="form-box">
							<div class="form-top">
								<div class="form-top-left">
									<h3>S'inscrire</h3>
								</div>
								<div class="form-top-right">
									<i class="fa fa-pencil fa-x5"></i>
								</div>
							</div>
							<div class="form-bottom">
								<%@ include file="../vues/fragment/signup_form.jspf"%>
								<c:if test="${not empty utilisateurAjouter}">
									<div class="alert alert-success">${utilisateurAjouter}</div>
								</c:if>

							</div>
						</div>

					</div>

				</div>

			</div>
		</div>

	</header>

	<%@ include file="../vues/fragment/about.jspf"%>

	<%@ include file="../vues/fragment/footer.jspf"%>

	<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
	<div
		class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
		<a class="btn btn-primary" href="#page-top"> <i
			class="fa fa-chevron-up"></i>
		</a>
	</div>

	<!-- jQuery -->
	<script src="resources/vendor/jquery/jquery.min.js"
		type="text/javascript"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"
		type="text/javascript"></script>

	<!-- Plugin JavaScript -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

	<!-- Contact Form JavaScript -->
	<script src="resources/js/jqBootstrapValidation.js"></script>
	<script src="resources/js/contact_me.js"></script>

	<!-- Theme JavaScript -->
	<script src="resources/js/freelancer.js" type="text/javascript"></script>

	<!-- Sign Up Form JavaScript -->
	<script src="resources/js/jquery.backstretch.js"></script>
	<script src="resources/js/scripts.js"></script>

</body>

</html>
