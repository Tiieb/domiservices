<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">

</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, shrink-to-fit=no, initial-scale=1">


<meta name="description" content="">
<meta name="author" content="">

<title>Mon profile</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Exo"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/stylesheet.css" rel="stylesheet">
<link href="resources/css/bootstrap-datepicker3.min.css"
	rel="stylesheet">
<link href="resources/css/profile1.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
	<div id="wrapper" class="toggled">

		<%@ include file="../vues/fragment/main_nav.jspf"%>
		<!-- Sidebar -->
		<%@ include file="../vues/fragment/side_nav.jspf"%>

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<button type="button" class="hamburger is-closed"
				style="margin-top: 50px;" data-toggle="offcanvas">
				<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
					class="hamb-bottom"></span>
			</button>
			<div class="container main col-md-11">
				<div class="col-md-12 fixed">
					<%@ include file="../vues/fragment/rechercheTrajet.jspf"%>
				</div>

				<div class="row well p_form col-md-11 ">
				<c:if test="${not empty UtilisateurModifie}">
						<div class="alert alert-success">${UtilisateurModifie}</div>
					</c:if>
					<div class="row profile">
						<div class="col-md-4">
							<div class="profile-sidebar">
								<!-- SIDEBAR USERPIC -->
								<div class="profile-userpic">
									<img class="" src="resources/img/iconProfile.png" alt="Profile"
										style="display: block; width: 60%; height: auto; margin-right: auto; , margin-left: auto;">
								</div>
								<!-- END SIDEBAR USERPIC -->
								<!-- SIDEBAR USER TITLE -->
								<div class="profile-usertitle">
									<div class="profile-usertitle-name">${sessionScope.utilisateurSession.nomUser}
										${sessionScope.utilisateurSession.prenomUser}</div>
								</div>
								<!-- END SIDEBAR USER TITLE -->
								<!-- SIDEBAR BUTTONS -->
								<div class="profile-userbuttons">
								<a href="messagerie" >
									<button type="button" class="btn btn-primary btn-lg"
										style="font-size:18px;">Messagerie</button></a>
							
								</div>
								<!-- END SIDEBAR BUTTONS -->
								<!-- SIDEBAR MENU -->
								<div class="profile-usermenu">
								
									${sessionScope.utilisateurSession.getEmailUtilisateur()} </br> 22ans
									</br> ${sessionScope.utilisateurSession.getAdresseUtilisateur()}
									</br> ${sessionScope.utilisateurSession.getTelUtilisateur()}
								</div>
								<!-- END MENU -->
							</div>
						</div>
						<div class="profile-cont col-md-8">



							<div class="col-md-8 row pro-info">
								<form:form modelAttribute="UtilisateurForm" id="userForm"
									method="GET" action="ModifierUser">

									<div class="pro-title">Modifier votre profil</div>
									
									<fieldset>
									<div class="form-group">
									
										Nom
										<form:input type="text" placeholder="${sessionScope.utilisateurSession.nomUser}"
											class="form-control" path="nom" />
									</div>

									<div class="form-group">
										<label class="sr-only" for="nom">Prenom</label>
										<form:input type="text" placeholder="${sessionScope.utilisateurSession.prenomUser}"
											class="form-control" path="prenom" />
									</div>


									<div class="form-group">
										<label class="sr-only" for="nom">T�l�phone</label>
										<form:input type="text" placeholder="${sessionScope.utilisateurSession.getTelUtilisateur()}"
											class="form-control" path="tel" />
									</div>

									<div class="form-group">
										<label class="sr-only" for="nom">Adresse</label>
										<form:input type="text" placeholder="${sessionScope.utilisateurSession.getAdresseUtilisateur()}"
											class="form-control" path="adresse" />
									</div>

									<div class="form-group">
										<label class="sr-only" for="nom">Email</label>
										<form:input type="text" placeholder="${sessionScope.utilisateurSession.getEmailUtilisateur()}"
											class="form-control" path="email" />
									</div>

									<div class="form-group">
										<label class="sr-only" for="nom">Mot de passe</label>
										<form:input type="text" placeholder="Saisissez un nouveau mot de passe"
											class="form-control" path="password" />
									</div>

									<button type="submit" class="btn btn-success"
										style="width: 100%; font-size: 18px; font-weight: bold">Valider</button>
										
										</fieldset>
								</form:form>

							</div>


						</div>
					</div>



				</div>




				<!-- /#page-content-wrapper -->

			</div>
			<!-- /#wrapper -->
		</div>
	</div>

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.fr.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Menu Toggle Script -->
	<script src="resources/js/side.js"></script>
	<script>
		$('#datep').datepicker({});
	</script>
	<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaIa3iiMjYNRq2e6NVQNKF4KU3v7pi77I&libraries=places&callback=initialize">
		
	</script>

	<script src="resources/js/autocomplete.js"></script>
</body>

</html>
