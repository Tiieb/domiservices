<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">

</c:if>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, shrink-to-fit=no, initial-scale=1">


<meta name="description" content="">
<meta name="author" content="">

<title>Profile de ${user_view.getNomUser()}
	${user_view.getPrenomUser()}</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Exo"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/stylesheet.css" rel="stylesheet">
<link href="resources/css/bootstrap-datepicker3.min.css"
	rel="stylesheet">
<link href="resources/css/profile1.css" rel="stylesheet">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>
	<div id="wrapper" class="toggled">

		<%@ include file="../vues/fragment/main_nav.jspf"%>
		<!-- Sidebar -->
		<%@ include file="../vues/fragment/side_nav.jspf"%>

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<button type="button" class="hamburger is-closed"
				style="margin-top: 50px;" data-toggle="offcanvas">
				<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
					class="hamb-bottom"></span>
			</button>
			<div class="container main col-md-11">
				<div class="col-md-12 fixed">
					<%@ include file="../vues/fragment/rechercheTrajet.jspf"%>
				</div>

				<div class="row  p_form col-md-11"
					style="font-size: 22px; padding: 0px;">

					<div class="well col-md-6 "style="font-size: 22px; text-align: center; background-color: #74a4f2; color: white">
						Messages personels
					</div>

					<a href="messagerieDemmande">
						<div class="well  col-md-6 msg_hover" style="font-size: 22px; text-align: center">
							Messages de demmandes
						</div>
					</a>
				</div>
				
				<div class=" row well  col-md-11 ">

				
					<div class="well col-md-12" style="font-size:18px;font-weight:bold;text-align:center;margin-bottom:10px">
							Messages Personels Recus
						</div>
				
					<div class="panel-group" id="accordion">

						<div class="row">
						</div>
						
						<c:if test="${empty message_perso_rec}">
							<div class="alert alert-danger">Vous n'avez aucun message
								recu</div>
						</c:if>
						<c:if test="${not empty message_perso_rec}">
							<c:forEach var="message" items="${message_perso_rec}"
								varStatus="loop">
								<div class="panel panel-default">
									<div class="panel-heading ">
										<h4 class="row">
											<div class="col-md-3 first_hed">
												<img class="" src="resources/img/iconProfile.png"
													alt="Profile"
													style="float: left; width: 40px; height: 40px; margin-right: 5px;">

												<a
													href="profile_view?idUser=${user_perso_rec.get(loop.index).getIdUser()}">
													${user_perso_rec.get(loop.index).getNomUser()} </br>
													${user_perso_rec.get(loop.index).getPrenomUser()}
												</a>
											</div>
											<div class="col-md-6 "></div>
											<div class="col-md-2 det"></div>
										</h4>
										<hr id="line">
										<div class="row" style="text-align: center;">
											<a data-toggle="collapse" data-parent="#accordion"
												href="#collapse${loop.index}"> Voir le message <span
												class="glyphicon glyphicon-plus"></span>
											</a>
										</div>
									</div>
									<div id="collapse${loop.index}" class="panel-collapse collapse">
										<div class="panel-body">${message.getMesage()}</div>
									</div>
								</div>
							</c:forEach>
						</c:if>

					</div>

					<div class="well col-md-12" style="font-size:18px;font-weight:bold;text-align:center;margin-bottom:10px">
							Messages Envoy�s
						</div>
						
					<div class="panel-group" id="accordion">

						<div class="row">
						</div>
						<c:if test="${empty message_perso_send}">
							<div class="alert alert-danger">Vous n'avez aucun message
								envoy�</div>
						</c:if>
						<c:if test="${not empty message_perso_send}">
							<c:forEach var="message" items="${message_perso_send}"
								varStatus="loop">
								<div class="panel panel-default">
									<div class="panel-heading ">
										<h4 class="row">
											<div class="col-md-3 first_hed">
												<img class="" src="resources/img/iconProfile.png"
													alt="Profile"
													style="float: left; width: 40px; height: 40px; margin-right: 5px;">

												<a
													href="profile_view?idUser=${user_perso_send.get(loop.index).getIdUser()}">
													${user_perso_send.get(loop.index).getNomUser()} </br>
													${user_perso_send.get(loop.index).getPrenomUser()}
												</a>
											</div>
											<div class="col-md-6 "></div>
											<div class="col-md-2 det"></div>
										</h4>
										<hr id="line">
										<div class="row" style="text-align: center;">
											<a data-toggle="collapse" data-parent="#accordion"
												href="#collapse${loop.index}"> Voir le message <span
												class="glyphicon glyphicon-plus"></span>
											</a>
										</div>
									</div>
									<div id="collapse${loop.index}" class="panel-collapse collapse">
										<div class="panel-body">${message.getMesage()}</div>
									</div>
								</div>
							</c:forEach>
						</c:if>

					</div>


				</div>




				<!-- /#page-content-wrapper -->

			</div>
			<!-- /#wrapper -->
		</div>
	</div>

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.fr.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Menu Toggle Script -->
	<script src="resources/js/side.js"></script>
	<script>
		$('#datep').datepicker({});
	</script>
	<script async defer
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDaIa3iiMjYNRq2e6NVQNKF4KU3v7pi77I&libraries=places&callback=initialize">
		
	</script>

	<script src="resources/js/autocomplete.js"></script>
</body>

</html>
