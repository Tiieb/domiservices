<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	isELIgnored="false" pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<c:if test="${not empty sessionScope.utilisateurSession.idUser}">
</c:if>

<html lang="fr">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, shrink-to-fit=no, initial-scale=1">


<meta name="description" content="">
<meta name="author" content="">


<title>Tableau de bord</title>

<!-- Bootstrap Core CSS -->
<link href="resources/vendor/bootstrap/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Exo"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Love+Ya+Like+A+Sister"
	rel="stylesheet">
<!-- Custom CSS -->
<link href="resources/css/simple-sidebar.css" rel="stylesheet">
<link href="resources/css/bootstrap-datepicker3.min.css"
	rel="stylesheet">
<link href="resources/css/stylesheet.css" rel="stylesheet">

</head>

<body>

	<div id="wrapper" class="toggled">

		<%@ include file="../vues/fragment/main_nav.jspf"%>
		<!-- Sidebar -->
		<%@ include file="../vues/fragment/side_nav.jspf"%>

		<!-- Page Content -->
		<div id="page-content-wrapper">
			<button type="button" class="hamburger is-closed"
				style="margin-top: 50px;" data-toggle="offcanvas">
				<span class="hamb-top"></span> <span class="hamb-middle"></span> <span
					class="hamb-bottom"></span>
			</button>
			<div class="main col-md-10">
				<div class="col-md-12 fixed">
					<%@ include file="../vues/fragment/rechercheTrajet.jspf"%>
				</div>
				<div class="row well p_form col-md-12"
					style="text-align: center; font-size: 22px;">Tableau de bord</div>

				<div class="row well col-md-12">
					<div id="map" class="row  col-md-8"></div>
					<div id="map-panel" class="row  col-md-4"
						style="height: 450px; overflow: auto; margin-top: 20px;"></div>
				</div>
				<div class="row well col-md-12">
					<c:if test="${not empty aucuneCommande}">
						<div class="alert alert-danger">${aucuneCommande}</div>
					</c:if>
					<c:if test="${not empty DemmandeAjouté}">
						<div class="alert alert-success">${DemmandeAjouté}</div>
					</c:if>
					<c:if test="${not empty coords}">
						<script language="javascript">
							var coordsArray = new Array();
							var adrArray = new Array();
							var adrDestArray = new Array();
							var latArray = new Array();
							var lonArray = new Array();
							var latDestArray = new Array();
							var lonDestArray = new Array();
						</script>
						<c:forEach var="coord" items="${coords}" varStatus="loop">

							<script language="javascript">
								var adr = "${coord.getAdresse()}"
								adrArray.push(adr);
								var adrDest = "${coord.getAdresseDest()}"
								adrDestArray.push(adrDest);
								var lat1 = ${coord.getLat()};
								latArray.push(lat1);
								var lon1 = ${coord.getLon()};
								lonArray.push(lon1);
								var latDest = ${coord.getLatDest()};
								latDestArray.push(latDest);
								var lonDest = ${coord.getLonDest()};
								lonDestArray.push(lonDest);
							</script>


						</c:forEach>
						<script src="resources/js/map.js"></script>
					</c:if>

					<div class="panel-group" id="accordion">

						<c:forEach var="commande" items="${ListeCommandesProche}"
							varStatus="loop">
							<div class="panel panel-default">
								<div class="panel-heading ">
									<h4 class="row">
										<div class="col-md-3 first_hed">
											<img class="" src="resources/img/iconProfile.png"
												alt="Profile"
												style="float: left; width: 40px; height: 40px; margin-right: 5px;">

											<a href="profile_view?idUser=${commande.getIdUser()}">
												${Users[loop.index].getNomUser()} </br>
												${Users[loop.index].getPrenomUser()}
											</a>
										</div>
										<div class="col-md-6 ">
											<div class="col-md-4 hed ">
												<div class="wel ">Depart :</div>
												${commande.positionColis}
											</div>
											<div class="col-md-4 hed">
												<div class="wel">Destination :</div>
												${commande.destinationColis}
											</div>
											<div class="col-md-4 hed">
												<div class="wel">Date :</div>

											</div>
										</div>
										<div class="col-md-2 det">
											<a
												href="AjouterDemmandeRun?idDemmande=${commande.idCommande}">
												<input type="button" value="Faire un demande"
												class="btn btn-primary">
											</a>
										</div>
									</h4>
									<hr id="line">
									<div class="row" style="text-align: center;">
										<a data-toggle="collapse" data-parent="#accordion"
											href="#collapse${loop.index}"> Plus <span
											class="glyphicon glyphicon-plus"></span>
										</a>
									</div>
								</div>
								<div id="collapse${loop.index}" class="panel-collapse collapse">
									<div class="panel-body">
										${sessionScope.utilisateurSession.idUser}</div>
								</div>
							</div>
						</c:forEach>

					</div>
				</div>
			</div>
		</div>
		<!-- /#page-content-wrapper -->

	</div>
	<!-- /#wrapper -->

	<!-- jQuery -->
	<script src="resources/js/jquery.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>
	<script src="resources/js/bootstrap-datepicker.fr.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="resources/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Menu Toggle Script -->
	<script src="resources/js/side.js"></script>
	<!-- 	Map Script -->

	<!-- Google API -->



</body>

</html>


