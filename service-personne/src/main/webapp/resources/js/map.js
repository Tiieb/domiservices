
function createMarker(oMarkers, lat, lon, pin, map) {
	oLatLng = new google.maps.LatLng(lat, lon);
	oMarker = new google.maps.Marker({
		position : oLatLng,
		map : map,
		icon : pin,
		title : "Course a faire"
	});
	oMarkers.push(oMarker);
	return oMarker;
}

function mapInitialize() {
	navigator.geolocation.getCurrentPosition(showPosition, showError);
}

function showPosition(position) {
	var oMarkers = [] ;
	var dMarkers = [] ;
	var directions = [] ;
	var infoBulles = [] ;
	var infoBullesDest = [] ;
	
	lat = position.coords.latitude;
	lon = position.coords.longitude;

	latlon = new google.maps.LatLng(lat, lon)
	var myOptions = {
		center : latlon,
		zoom : 13,
		mapTypeId : google.maps.MapTypeId.ROADMAP,
		mapTypeControl : false,
		navigationControlOptions : {
			style : google.maps.NavigationControlStyle.SMALL
		}
	}
	var map = new google.maps.Map(document.getElementById("map"), myOptions);
	var marker = new google.maps.Marker({
		position : latlon,
		map : map,
		title : "Vous etes ici!"
	});

	var pinColor = "1BAAF7";

	var pinImage = new google.maps.MarkerImage(
			"http://maps.google.com/mapfiles/dd-start.png", new google.maps.Size(21, 34),
			new google.maps.Point(0, 0), new google.maps.Point(10, 34));
	var oLatLng, oMarker;
	for (i = 0; i < latArray.length; i++) {
		
		var oMarker = createMarker(oMarkers, latArray[i], lonArray[i], pinImage, map);

		var contenuInfoBulle = '<div class="panel panel-default"><h5>Depart : </h5>'
				+ adrArray[i] + '</div>';
		var contenuInfoBulleDest = '<div class="panel panel-default"><h5>Destination : </h5>'
			+ adrArray[i] + '</div>';
		var infoBulle = new google.maps.InfoWindow();
		var infoBulleDest = new google.maps.InfoWindow();
		
		var latDest = latDestArray[i];
		var lonDest = lonDestArray[i];
		var lat2 = latArray[i];
		var lon2 = lonArray[i];

		google.maps.event
				.addListener(
						oMarker,
						'click',
						(function(oMarker, contenuInfoBulle, infoBulle, infoBulleDest, contenuInfoBulleDest,
								latDest, lonDest, lat2, lon2) {
							
							return function() {
								for(var j=0;j<dMarkers.length; j++){
									dMarkers[j].setMap(null);
									directions[j].setMap(null);
									directions[j].setPanel(null);
									if(infoBulles[j])
									infoBulles[j].close();
									if(infoBullesDest[j])
									infoBullesDest[j].close();
								}
								infoBulle.setContent(contenuInfoBulle);
								infoBulle.open(map, oMarker);
								infoBulles.push(infoBulle) ;

								var dLatLng, dMarker;
								var pinImage1 = new google.maps.MarkerImage(
										"http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|42b0f4",
										new google.maps.Size(21, 34),
										new google.maps.Point(0, 0),
										new google.maps.Point(10, 34));

								dLatLng = new google.maps.LatLng(latDest,
										lonDest);
								var dMarker = new google.maps.Marker({
									position : dLatLng,
									map : map,
									icon : pinImage1,
									label : "Destination",
									animation : google.maps.Animation.DROP,
									title : "Course a faire"
								});
								dMarkers.push(dMarker);
								infoBulleDest.setContent(contenuInfoBulleDest);
								infoBulleDest.open(map, dMarker);
								infoBullesDest.push(infoBulleDest) ;

								setTimeout(
										function() {
											dMarker
													.setAnimation(google.maps.Animation.BOUNCE);
										}, 640);

								var direction = new google.maps.DirectionsRenderer();
								direction.setOptions({
								    map: map,
								    panel: document.getElementById('map-panel'),
								    suppressMarkers: true,
								    preserveViewport: true,
								});
									

								if (lat && latDest && lon && lonDest) {
									var request = {
										origin : new google.maps.LatLng(lat2, lon2),
										destination : new google.maps.LatLng(latDest, lonDest),
										travelMode : google.maps.DirectionsTravelMode.DRIVING,
										provideRouteAlternatives : true,
										unitSystem: google.maps.UnitSystem.METRIC,
									}

									var directionsService = new google.maps.DirectionsService(); 
									directionsService.route(request,
													function(response, status) { 
														if (status == google.maps.DirectionsStatus.OK) {
															direction.setDirections(response);
														}
													});
									directions.push(direction);
								}

								google.maps.event.addListener(infoBulle,
										'closeclick', function() {
											dMarker.setMap(null);
											direction.setMap(null);
											direction.setPanel(null);
										});
								
								google.maps.event.addListener(infoBulleDest,
										'closeclick', function() {
											dMarker.setMap(null);
											infoBulle.setMap(null);
											direction.setMap(null);
											direction.setPanel(null);
										});	

							};
						})(oMarker, contenuInfoBulle, infoBulle, infoBulleDest, contenuInfoBulleDest,  latDest,
								lonDest, lat2,lon2));

	}
}

function showError(error) {
	switch (error.code) {
	case error.PERMISSION_DENIED:
		x.innerHTML = "User denied the request for Geolocation."
		break;
	case error.POSITION_UNAVAILABLE:
		x.innerHTML = "Location information is unavailable."
		break;
	case error.TIMEOUT:
		x.innerHTML = "The request to get user location timed out."
		break;
	case error.UNKNOWN_ERROR:
		x.innerHTML = "An unknown error occurred."
		break;
	}
}
