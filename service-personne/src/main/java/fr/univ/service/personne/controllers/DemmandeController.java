package fr.univ.service.personne.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.SessionScope;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.univ.service.personne.form.AjouterCommandeForm;

import fr.univ.service.personne.form.RechercheTrajetForm;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;
import fr.univ.service.personne.services.CommandeService;
import fr.univ.service.personne.services.DemmandeService;

@Controller
@Scope("session")
@SessionAttributes(value = "utilisateurSession", types = { UtilisateurEntity.class })
public class DemmandeController {

	@Autowired
	DemmandeService mDemmandeService;

	@RequestMapping(value = "**/AjouterDemmandeRun", method = RequestMethod.GET)
	public ModelAndView ajouterDemmandeRun(
			@RequestParam("idDemmande") int idDemmande,
			HttpServletRequest request,
			@ModelAttribute("ajouterCommandeForm") AjouterCommandeForm pAjouterCommandeForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		int id = pAjouterCommandeForm.getIdUser();
		
		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		System.out.println(user.getIdUser());
		if (mDemmandeService.ajouterDemmande(idDemmande, user.getIdUser())) {

			vRedirectMap.put("DemmandeAjout�", "La demmande a �t� enregistr� avec succ�s");
			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:espacePersoDisplay");
			return pModelAndView;

		} else {

			vRedirectMap.put("ErreurAjoutDemmande", "une erreur de serveur s'est produite. votre demmande n'a pas �t� enregistr� ");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:espacePersoDisplay");
			return pModelAndView;

		}

	}
	
	@RequestMapping(value = "**/AccepterDemmande", method = RequestMethod.GET)
	public ModelAndView acceterDemmade(
			@RequestParam("idDemmande") int idDemmande,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		 
		if (mDemmandeService.setEtat(idDemmande, "En cours")) {
			mDemmandeService.deleteAll(idDemmande);
			vRedirectMap.put("DemmandeAccept�","La demmande a bien �t� accept�" );

		} else {

			vRedirectMap.put("DemmandeRejet�","La demmande a �t� rejet�" );
		}
		
		pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
		pModelAndView.setViewName("redirect:MesCommandes");
		return pModelAndView;

	}
	

}
