package fr.univ.service.personne.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "commande")
public class CommandeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_commande")
	private int idCommande;

	@Column(name = "type_commande")
	private String TypeCommande;

	@Column(name = "position_colis")
	private String positionColis;

	@Column(name = "destination_colis")
	private String destinationColis;

	@Column(name = "fk_utilisateur")
	private int idUser;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public int getIdCommande() {
		return idCommande;
	}

	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}

	public String getTypeCommande() {
		return TypeCommande;
	}

	public void setTypeCommande(String typeCommande) {
		TypeCommande = typeCommande;
	}

	public String getPositionColis() {
		return positionColis;
	}

	public void setPositionColis(String positionColis) {
		this.positionColis = positionColis;
	}

	public String getDestinationColis() {
		return destinationColis;
	}

	public void setDestinationColis(String destinationColis) {
		this.destinationColis = destinationColis;
	}

}
