package fr.univ.service.personne.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.univ.service.personne.dao.UserDao;
import fr.univ.service.personne.form.ConnexionForm;
import fr.univ.service.personne.form.UtilisateurForm;
import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;

@Repository
public class UtilisateurService {

	@Autowired
	UserDao userDao;

	public UtilisateurService() {
		// TODO Auto-generated constructor stub
	}
	
	
	public boolean addUser(UtilisateurForm pUtilisateurForm){
		
		UtilisateurEntity pUtilisateurEntity=new UtilisateurEntity();
		
		pUtilisateurEntity.setNomUser(pUtilisateurForm.getNom());
		pUtilisateurEntity.setPrenomUser(pUtilisateurForm.getPrenom());
		pUtilisateurEntity.setTelUtilisateur(pUtilisateurForm.getTel());
		pUtilisateurEntity.setAdresseUtilisateur(pUtilisateurForm.getAdresse());
		pUtilisateurEntity.setEmailUtilisateur(pUtilisateurForm.getEmail());
		pUtilisateurEntity.setPasswordUtilisateur(pUtilisateurForm.getPassword());
		
		UtilisateurEntity vUtilisateurEntityuser=new UtilisateurEntity();
		
		vUtilisateurEntityuser=userDao.create(pUtilisateurEntity);
		if(null!= vUtilisateurEntityuser){
			
			return true;
		}else{
			
		return false;}
	}
	
public boolean modifier(int idUser, UtilisateurForm UtilisateurForm){
		
		UtilisateurEntity user = userDao.find(idUser);
		
		if(!UtilisateurForm.getNom().isEmpty()){
			user.setNomUser(UtilisateurForm.getNom());
		}
		if(!UtilisateurForm.getPrenom().isEmpty()){
			user.setPrenomUser(UtilisateurForm.getPrenom());
		}
		if(!UtilisateurForm.getAdresse().isEmpty()){
			user.setAdresseUtilisateur(UtilisateurForm.getAdresse());
		}
		if(!UtilisateurForm.getEmail().isEmpty()){
			user.setEmailUtilisateur(UtilisateurForm.getEmail());
		}
		if(!UtilisateurForm.getPassword().isEmpty()){
			user.setPasswordUtilisateur(UtilisateurForm.getPassword());
		}
		if(!UtilisateurForm.getTel().isEmpty()){
			user.setTelUtilisateur(UtilisateurForm.getTel());
		}
		
		userDao.update(user);
			return true;
	}

	
	public List<UtilisateurEntity> getUserPerDemmande(int idDemmande) {
		List<UtilisateurEntity> vListUser = userDao.getUserPerDemmande(idDemmande);
		
		if (null != vListUser) {
			System.out.println("liste demmande n'est pas vide");
			
		} else {
			System.out.println("lise demmande vide");
		}
		

		return vListUser;

	}
	
	
	
	public UtilisateurEntity getConnexionUtilisateur(ConnexionForm pConnexionForm){
		
	UtilisateurEntity vUtilisateur =userDao.getUtilisateurConnexion(pConnexionForm.getEmail(),pConnexionForm.getPassword());
		
		return vUtilisateur;
		
	}
	
	public UtilisateurEntity getUser(int idUser){
		
		UtilisateurEntity vUtilisateur = userDao.getUtilisateur(idUser);
			
			return vUtilisateur;
			
		}


	public UserDao getUserDao() {
		return userDao;
	}


	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
	
	
}
