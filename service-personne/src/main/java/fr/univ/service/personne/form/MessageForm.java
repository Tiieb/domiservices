package fr.univ.service.personne.form;

import java.util.List;

import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;

public class MessageForm {
	
	private String text;
	private int idUser;
	private int idDemmande;

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getIdDemmande() {
		return idDemmande;
	}

	public void setIdDemmande(int idDemmande) {
		this.idDemmande = idDemmande;
	}

}
