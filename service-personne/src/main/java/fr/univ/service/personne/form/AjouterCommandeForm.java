package fr.univ.service.personne.form;

public class AjouterCommandeForm {
	
	
	
	private int idCommande;
	private String typeCommande;
	private String positionColis;
	private String destinationColis;
	private int idUser;
	
	
	public int getIdUser() {
		return idUser;
	}
	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}
	public int getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}
	public String getTypeCommande() {
		return typeCommande;
	}
	public void setTypeCommande(String typeCommande) {
		this.typeCommande = typeCommande;
	}
	public String getPositionColis() {
		return positionColis;
	}
	public void setPositionColis(String positionColis) {
		this.positionColis = positionColis;
	}
	public String getDestinationColis() {
		return destinationColis;
	}
	public void setDestinationColis(String destinationColis) {
		this.destinationColis = destinationColis;
	}
	
	

	
	
	
	
}
