package fr.univ.service.personne.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.univ.service.personne.form.AjouterTrajetForm;
import fr.univ.service.personne.form.RechercheTrajetForm;
import fr.univ.service.personne.form.RecupererTousLesTrajetsForm;
import fr.univ.service.personne.model.TrajetEntity;
import fr.univ.service.personne.services.TrajetService;
import fr.univ.service.personne.services.UtilisateurService;

@Controller
public class TrajetController {
	@Autowired
	TrajetService mTrajetService;

	@Autowired
	UtilisateurService mUtilisateurService;

	@RequestMapping(value = "**/AjouterTrajetAffichage", method = RequestMethod.GET)
	public ModelAndView ajouterTrajet(@ModelAttribute("ajouterTrajetForm") AjouterTrajetForm pAjouterTrajetForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap pRedirectMapAttribut, ModelAndView pModelAndView) {
		if (pRedirectMapAttribut.containsKey("TajetAjouter")) {

			pModelAndView.getModelMap().addAttribute("TajetAjouter", (String) pRedirectMapAttribut.get("TajetAjouter"));

		}
		if (pRedirectMapAttribut.containsKey("ErreurAjoutTrajet")) {

			pModelAndView.getModelMap().addAttribute("ErreurAjoutTrajet",
					(String) pRedirectMapAttribut.get("ErreurAjoutTrajet"));

		}
		pModelAndView.setViewName("ajouterTrajet");
		return pModelAndView;

	}

	@RequestMapping(value = "**/AjouterTrajetRun", method = RequestMethod.GET)
	public ModelAndView ajouterTrajetRun(@ModelAttribute("ajouterTrajetForm") AjouterTrajetForm pAjouterTrajetForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		if (mTrajetService.ajouterTrajet(pAjouterTrajetForm)) {

			vRedirectMap.put("TajetAjouter", "Le trajet a �t� enregistr� avec succ�s");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:AjouterTrajetAffichage");

			return pModelAndView;

		} else {

			vRedirectMap.put("ErreurAjoutTrajet",
					"une erreur de serveur s'est produite. votre trajet n'a pas �t� enregistr� ");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:redirect:AjouterTrajetAffichage");
			return pModelAndView;

		}

	}

	@RequestMapping(value = "**/RechercherTrajetRun", method = RequestMethod.GET)
	public ModelAndView recherchertrajetRun(@ModelAttribute("ajouterTrajetForm") AjouterTrajetForm pAjouterTrajetForm,
			@ModelAttribute("rechercherTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			ModelAndView pModelAndView) {

		if (null != mTrajetService.rechercherTrajet(pRechercheTrajetForm)) {
			System.out.println("liste n'est pas vide");
		} else {
			System.out.println("lise vide");
		}

		pModelAndView.setViewName("ajouter_trajet");
		return pModelAndView;

	}

	@RequestMapping(value = "**/recupererTousLesTrajetAffichage", method = RequestMethod.GET)
	public ModelAndView recupererTousLesTrajetAffichage(
			@ModelAttribute("recupererTousLesTrajetsForm") RecupererTousLesTrajetsForm pRecupererTousLesTrajetsForm,
			@ModelAttribute("redirectMapAttribut") HashMap pRedirectMapAttribut,
			@ModelAttribute("rechercherTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			ModelAndView pModelAndView) {

		if (pRedirectMapAttribut.containsKey("listeDesTrajets")) {
			pModelAndView.getModelMap().addAttribute("listeDesTrajets",
					(List<TrajetEntity>) pRedirectMapAttribut.get("listeDesTrajets"));
		}

		if (pRedirectMapAttribut.containsKey("listeVide")) {
			pModelAndView.getModelMap().addAttribute("listeVide", (String) pRedirectMapAttribut.get("listeVide"));
		}
		if (pRedirectMapAttribut.containsKey("messageInfo")) {

			pModelAndView.getModelMap().addAttribute("messageInfo", (String) pRedirectMapAttribut.get("messageInfo"));

		}

		pModelAndView.setViewName("mesTrajets");
		return pModelAndView;
	}

	@RequestMapping(value = "**/recupererTousLesTrajet", method = RequestMethod.GET)
	public ModelAndView recupererTousLesTrajetRun(@RequestParam("idUser") int idUser,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {
		System.out.println(idUser);
		List<TrajetEntity> vListeDesTrajets = mTrajetService.getTousTrajet(idUser);
		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		if (null != vListeDesTrajets) {

			vRedirectMap.put("listeDesTrajets", vListeDesTrajets);

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:recupererTousLesTrajetAffichage");

			return pModelAndView;

		}
		vRedirectMap.put("listeVide", "La liste des trajets est vide");
		pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
		pModelAndView.setViewName("redirect:recupererTousLesTrajetAffichage");
		return pModelAndView;
	}

	@RequestMapping(value = "**/supprimerTrajet", method = RequestMethod.GET)
	public ModelAndView supprimerTrajet(@RequestParam("idTrajet") int idTrajet,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {
		Map<String, Object> vRedirectMap = new HashMap<String, Object>();

		if (mTrajetService.supprimerTrajet(idTrajet)) {

			vRedirectMap.put("messageInfo", "Le trajet a �t� supprim�");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:recupererTousLesTrajetAffichage");

			return pModelAndView;
		} else {

			vRedirectMap.put("messageErreur", "Le trajet n'a pas �t� supprim�");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:recupererTousLesTrajetAffichage");

			return pModelAndView;
		}
	}

}
