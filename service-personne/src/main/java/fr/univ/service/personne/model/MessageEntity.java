package fr.univ.service.personne.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "message")
public class MessageEntity {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_message")
	private int idMessage ;
	
	@Column(name = "id_demmande")
	private int idDemmande ;
	
	@Column(name = "id_user_send")
	private int idUserSend ;
	
	@Column(name = "id_user_rec")
	private int idUserRec ;
	
	@Column(name = "message")
	private String mesage ;
	
	
	public int getIdMessage() {
		return idMessage;
	}
	public void setIdMessage(int idMessage) {
		this.idMessage = idMessage;
	}
	public int getIdDemmande() {
		return idDemmande;
	}
	public void setIdDemmande(int idDemmande) {
		this.idDemmande = idDemmande;
	}
	public int getIdUserSend() {
		return idUserSend;
	}
	public void setIdUserSend(int idUserSend) {
		this.idUserSend = idUserSend;
	}
	public int getIdUserRec() {
		return idUserRec;
	}
	public void setIdUserRec(int idUserRec) {
		this.idUserRec = idUserRec;
	}
	public String getMesage() {
		return mesage;
	}
	public void setMesage(String mesage) {
		this.mesage = mesage;
	}
	
	
	
}
