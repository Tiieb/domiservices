package fr.univ.service.personne.form;

import java.util.List;

import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;

public class DemmandeUser {
	
	private List<UtilisateurEntity> userList ;
	private List<DemmandeEntity> demmandeList ;
	
	
	public List<UtilisateurEntity> getUserList() {
		return userList;
	}
	public void setUserList(List<UtilisateurEntity> userList) {
		this.userList = userList;
	}
	public List<DemmandeEntity> getDemmandeList() {
		return demmandeList;
	}
	public void setDemmandeList(List<DemmandeEntity> demmandeList) {
		this.demmandeList = demmandeList;
	}
	
}
