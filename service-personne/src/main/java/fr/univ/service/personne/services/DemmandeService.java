package fr.univ.service.personne.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.univ.service.personne.dao.CommandeDao;
import fr.univ.service.personne.dao.DemmandeDao;
import fr.univ.service.personne.dao.UserDao;
import fr.univ.service.personne.form.AjouterCommandeForm;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.form.DemmandeUser;
import fr.univ.service.personne.model.TrajetEntity;
import fr.univ.service.personne.model.UtilisateurEntity;

@Repository
public class DemmandeService {

	@Autowired
	DemmandeDao mDemmandeDao;
	
	@Autowired
	UserDao mUserDao;

	public boolean ajouterDemmande(int idCommande, int idUser) {

		DemmandeEntity vDemmandeEntity = new DemmandeEntity();
		vDemmandeEntity.setIdCommande(idCommande);
		vDemmandeEntity.setIdUser(idUser);
		vDemmandeEntity.setEtat("En attente");
		
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"; 
	    String mdp = "";
	    for(int x=0;x<8;x++)
	    {
	       int i = (int)Math.floor(Math.random() * 34); 
	       mdp += chars.charAt(i);
	    }
	    
		vDemmandeEntity.setMdp(mdp);
		
		DemmandeEntity retourDemmandeEntity = new DemmandeEntity();

		retourDemmandeEntity = mDemmandeDao.create(vDemmandeEntity);
		if (null != retourDemmandeEntity) {
			return true;

		} else {
			return false;
		}
	}

	public DemmandeUser getDemmandesPerCommande(int idCommande) {
		DemmandeUser demandeUser = new DemmandeUser();
		
		List<DemmandeEntity> vListDemmande = mDemmandeDao.getDemmandesPerCommande(idCommande);
		List<UtilisateurEntity> vUsers = new ArrayList<UtilisateurEntity>();
		
		for(int i=0;i<vListDemmande.size();i++){
			
			
			UtilisateurEntity vUserPerDemmande = mUserDao.getUtilisateur(vListDemmande.get(i).getIdUser());
			
			vUsers.add(vUserPerDemmande);
			
			
		}
		Collections.reverse(vUsers);
		
		demandeUser.setDemmandeList(vListDemmande);
		demandeUser.setUserList(vUsers);
		
		if (null != vListDemmande) {

			System.out.println("liste demmande n'est pas vide");
		} else {
			System.out.println("lise demmande vide");
		}
		
		Collections.reverse(vListDemmande);

		return demandeUser;

	}
	
	public boolean setEtat(int idDemmande, String etat) {
		
		if(mDemmandeDao.setEtat(idDemmande, etat)){
			System.out.println("demmande accept�");
			return true ; 
		}else  {
			System.out.println("non accept�");
			return false;
		}
}
	
	public void deleteAll(int id){
		mDemmandeDao.deleteAll(id);
	}
	
	
	public void supprimerDemmande(int id){
		mDemmandeDao.delete(id);
	}
	
//	public void modifierDemmande(int id, DemmandeEntity commande ){
//		DemmandeEntity com = mDemmandeDao.getDemmandeById(id) ;
//		com.setIdDemmande(commande.getIdDemmande());
//		com.setDestinationColis(commande.getDestinationColis());
//		com.setIdUser(commande.getIdUser());
//		com.setPositionColis(commande.getPositionColis());
//		mDemmandeDao.update(com);
//	}

}
