package fr.univ.service.personne.dao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


public abstract class GenericDaoAbstract<T> implements IGenericDao<T> {

	@PersistenceContext
	protected EntityManager em;

	private Class<T> type;

	public GenericDaoAbstract() {
		Type t = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) t;
		type = (Class) pt.getActualTypeArguments()[0];
	}

	
	public T create(final T t) {
		this.em.persist(t);
		return t;
	}
	
	public void delete(final Object id) {
		this.em.remove(this.em.getReference(type, id));
	}
	
	public T find(final Object id) {
		return (T) this.em.find(type, id);
	}
	
	public T update(final T t) {
		return this.em.merge(t);
	}


	public EntityManager getEm() {
		return em;
	}


	public void setEm(EntityManager em) {
		this.em = em;
	}
}