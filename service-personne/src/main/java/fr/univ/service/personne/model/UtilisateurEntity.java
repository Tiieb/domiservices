package fr.univ.service.personne.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.context.annotation.Scope;

@Entity
@Scope("session")
@Table(name ="utilisateur")
public class UtilisateurEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_utilisateur")	
	private int idUser;
	
	@Column(name="nom_utilisateur")
	private String nomUser;
	
	@Column(name="prenom_utilisateur")
	private String prenomUser;
	
	@Column(name="tel_utilisateur")
	private String telUtilisateur;
	
	@Column(name="adresse_utilisateur")
	private String adresseUtilisateur;
	
	@Column(name="email_utilisateur")
	private String emailUtilisateur;
	
	@Column(name="password")
	private String passwordUtilisateur;
	
	@OneToMany()
	private List<TrajetEntity> trajets;

	
	
	
	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getNomUser() {
		return nomUser;
	}

	public void setNomUser(String nomUser) {
		this.nomUser = nomUser;
	}

	public String getPrenomUser() {
		return prenomUser;
	}

	public void setPrenomUser(String prenomUser) {
		this.prenomUser = prenomUser;
	}

	public String getTelUtilisateur() {
		return telUtilisateur;
	}

	public void setTelUtilisateur(String telUtilisateur) {
		this.telUtilisateur = telUtilisateur;
	}

	public String getAdresseUtilisateur() {
		return adresseUtilisateur;
	}

	public void setAdresseUtilisateur(String adresseUtilisateur) {
		this.adresseUtilisateur = adresseUtilisateur;
	}
	
	

	public String getEmailUtilisateur() {
		return emailUtilisateur;
	}

	public void setEmailUtilisateur(String emailUtilisateur) {
		this.emailUtilisateur = emailUtilisateur;
	}

	public String getPasswordUtilisateur() {
		return passwordUtilisateur;
	}

	public void setPasswordUtilisateur(String passwordUtilisateur) {
		this.passwordUtilisateur = passwordUtilisateur;
	}

	public List<TrajetEntity> getTrajets() {
		return trajets;
	}

	public void setTrajets(List<TrajetEntity> trajets) {
		this.trajets = trajets;
	}
	
	

	

	
	
	
	

}
