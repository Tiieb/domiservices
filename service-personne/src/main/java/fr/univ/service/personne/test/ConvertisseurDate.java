package fr.univ.service.personne.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConvertisseurDate {

	
	public static Date stringToDate(String pStringDate, String pFormatDate) {

		DateFormat df = new SimpleDateFormat(pFormatDate);
		Date result = null;
		try {
			result = df.parse(pStringDate);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}
}
