package fr.univ.service.personne.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.univ.service.personne.dao.CommandeDao;
import fr.univ.service.personne.dao.DemmandeDao;
import fr.univ.service.personne.dao.MessageDao;
import fr.univ.service.personne.dao.UserDao;
import fr.univ.service.personne.form.AjouterCommandeForm;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.MessageEntity;
import fr.univ.service.personne.model.MessagePersoEntity;
import fr.univ.service.personne.form.DemmandeUser;
import fr.univ.service.personne.form.MessageBean;
import fr.univ.service.personne.model.TrajetEntity;
import fr.univ.service.personne.model.UtilisateurEntity;

@Repository
public class MessageService {

	@Autowired
	MessageDao mMessageDao;

	@Autowired
	UserDao mUserDao;
	
	@Autowired
	DemmandeDao mDemmandeDao;

	public boolean ajouterMessage(int idDemmande, int idUser, String message) {
		
		System.out.println("service (demande id) "+mDemmandeDao.getDemmandeById(idDemmande).getIdDemmande());
		System.out.println("service (user demmandeur id) "+mDemmandeDao.getDemmandeById(idDemmande).getIdUser());

		MessageEntity vMessageEntity = new MessageEntity();
		vMessageEntity.setIdDemmande(idDemmande);
		vMessageEntity.setMesage(message);
		vMessageEntity.setIdUserRec(mDemmandeDao.getDemmandeById(idDemmande).getIdUser());
		vMessageEntity.setIdUserSend(idUser);


		MessageEntity retourMessageEntity = new MessageEntity();

		retourMessageEntity = mMessageDao.create(vMessageEntity);
		if (null != retourMessageEntity) {
			return true;

		} else {
			return false;
		}
	}
	
	
	public MessageBean getMesMessages(int pIdUser) {

		MessageBean vListMessage = mMessageDao.getMesMessages(pIdUser);
		if (null != vListMessage) {

			System.out.println("liste commandes perso n'est pas vide");
		} else {
			System.out.println("liste commandes perso vide");
		}
		
		Collections.reverse(vListMessage.getMessage_send());
		Collections.reverse(vListMessage.getMessage_rec());
		Collections.reverse(vListMessage.getMessage_perso_send());
		Collections.reverse(vListMessage.getMessage_perso_send());
		
		

		return vListMessage;

	}

	public boolean ajouterMessagePerso(int idUser_rec, int idUser, String message) {

		MessagePersoEntity vMessageEntity = new MessagePersoEntity();
		vMessageEntity.setMesage(message);
		vMessageEntity.setIdUserRec(idUser_rec);
		vMessageEntity.setIdUserSend(idUser);


		MessagePersoEntity retourMessageEntity = new MessagePersoEntity();

		retourMessageEntity = mMessageDao.create(vMessageEntity);
		if (null != retourMessageEntity) {
			return true;

		} else {
			return false;
		}
	}

	public boolean setEtat(int idMessage, String etat) {

		if (mMessageDao.setEtat(idMessage, etat)) {
			System.out.println("demmande accept�");
			return true;
		} else {
			System.out.println("non accept�");
			return false;
		}
	}

	public void supprimerMessage(int id) {
		mMessageDao.delete(id);
	}

	// public void modifierMessage(int id, MessageEntity commande ){
	// MessageEntity com = mMessageDao.getMessageById(id) ;
	// com.setIdMessage(commande.getIdMessage());
	// com.setDestinationColis(commande.getDestinationColis());
	// com.setIdUser(commande.getIdUser());
	// com.setPositionColis(commande.getPositionColis());
	// mDemmandeDao.update(com);
	// }

}
