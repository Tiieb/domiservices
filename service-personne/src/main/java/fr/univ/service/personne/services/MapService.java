package fr.univ.service.personne.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderGeometry;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;

import fr.univ.service.personne.dao.UserDao;
import fr.univ.service.personne.form.ConnexionForm;
import fr.univ.service.personne.form.UtilisateurForm;
import fr.univ.service.personne.form.AdresseBean;
import fr.univ.service.personne.model.UtilisateurEntity;

public class MapService {
	
	public AdresseBean convertAdresse(String adresse, String adresseDest){
		
		AdresseBean adr = new AdresseBean() ;
		
		Geocoder geocoder = new Geocoder() ;
		GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().setAddress(adresse).getGeocoderRequest();
	    GeocodeResponse geocodeResponse = geocoder.geocode(geocoderRequest);
	    if(geocodeResponse!=null){
	    	System.out.println("existe");
	    }else{
	    	System.out.println("n'existe pas");
	    }

	    List<GeocoderResult> results = geocodeResponse.getResults();
	    adr.setLat(results.get(0).getGeometry().getLocation().getLat().floatValue());
	    adr.setLon(results.get(0).getGeometry().getLocation().getLng().floatValue());
	    
	    geocoderRequest = new GeocoderRequestBuilder().setAddress(adresseDest).getGeocoderRequest();
	    geocodeResponse = geocoder.geocode(geocoderRequest);
	    results = geocodeResponse.getResults();
	    adr.setLatDest(results.get(0).getGeometry().getLocation().getLat().floatValue());
	    adr.setLonDest(results.get(0).getGeometry().getLocation().getLng().floatValue());
	    
	    adr.setAdresse(adresse);
	    adr.setAdresseDest(adresseDest);
	    
		return adr;
	}	
}
