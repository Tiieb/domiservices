package fr.univ.service.personne.model;

import java.util.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "trajet")
public class TrajetEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_trajet")
	private int idTrajet;

	@Column(name = "ville_depart")
	private String villeDepart;

	@Column(name = "ville_destination")
	private String villeDestination;

	@Column(name = "date_depart")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateDepart;

	@Column(name = "ville_intermediaire")
	private String villeIntermediaire;

	@Column(name = "fk_utilisateur")
	private int idUser;

	public String getVilleDepart() {
		return villeDepart;
	}

	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}

	public String getVilleDestination() {
		return villeDestination;
	}

	public void setVilleDestination(String villeDestination) {
		this.villeDestination = villeDestination;
	}

	public String getVilleIntermediaire() {
		return villeIntermediaire;
	}

	public void setVilleIntermediaire(String villeIntermediaire) {
		this.villeIntermediaire = villeIntermediaire;
	}

	public Date getDateDepart() {
		return dateDepart;
	}

	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}

	public int getIdTrajet() {
		return idTrajet;
	}

	public void setIdTrajet(int idTrajet) {
		this.idTrajet = idTrajet;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

}
