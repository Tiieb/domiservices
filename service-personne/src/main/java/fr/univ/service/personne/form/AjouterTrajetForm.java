package fr.univ.service.personne.form;


public class AjouterTrajetForm {
	
	
	private int idUtilisateur;
	private String villeDepart;
	private String villeIntermediaire;
	private String villeDestination;
	private String dateDepart;
	
	
	
	public int getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(int idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getVilleDepart() {
		return villeDepart;
	}
	public void setVilleDepart(String villeDepart) {
		this.villeDepart = villeDepart;
	}
	public String getVilleDestination() {
		return villeDestination;
	}
	public void setVilleDestination(String villeDestination) {
		this.villeDestination = villeDestination;
	}
	public String getVilleIntermediaire() {
		return villeIntermediaire;
	}
	public void setVilleIntermediaire(String villeIntermediaire) {
		this.villeIntermediaire = villeIntermediaire;
	}
	public String getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}
	

	
	

}
