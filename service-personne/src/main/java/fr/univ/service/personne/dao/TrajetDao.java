package fr.univ.service.personne.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.univ.service.personne.model.TrajetEntity;
import fr.univ.service.personne.model.UtilisateurEntity;


@Repository
public class TrajetDao {
	
	@PersistenceContext
	EntityManager em;
	
	public TrajetDao() {
		// TODO Auto-generated constructor stub
	}
	
	@Transactional
	public TrajetEntity create(final TrajetEntity pTrajetEntity) {
		em.persist(pTrajetEntity);
		return pTrajetEntity;
	}
	@Transactional
	public void delete(final Object id) {
		this.em.remove(em.getReference(TrajetEntity.class, id));
	}
	
	
	public List<TrajetEntity> getTrajetRecherche(String pVilleDepart, String pVilleDestination,Date pDate ){
		
		Query vQuery = em.createQuery("SELECT U FROM TrajetEntity U WHERE U.villeDepart = :villeDepart and U.villeDestination = :villeDestination and U.date = :date");
		vQuery.setParameter("villeDepart", pVilleDepart);
		vQuery.setParameter("villeDestination", pVilleDestination);
		vQuery.setParameter("date",pDate);
		
		return (List<TrajetEntity>) vQuery.getResultList();
		
	}

public List<TrajetEntity> getTrajets(int pIdUser){
		
		Query vQuery = em.createQuery("SELECT U FROM TrajetEntity U WHERE U.idUser = :idUser");
		vQuery.setParameter("idUser", pIdUser);
		
		return (List<TrajetEntity>) vQuery.getResultList();
		
	}

public boolean supprimerTrajets(int pIdTrajet){
	
	TrajetEntity vTrajetEntity =new TrajetEntity();
	vTrajetEntity.setIdTrajet(pIdTrajet);
	Query vQuery = em.createQuery("delete U FROM TrajetEntity U WHERE U.idTrajet = :idTrajet");
	vQuery.setParameter("idTrajet", pIdTrajet);
	vQuery.executeUpdate();
	return true;
	
}







	
	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
	

}
