package fr.univ.service.personne.form;

public class AdresseBean {
	
	
	public float getLatDest() {
		return latDest;
	}
	public void setLatDest(float latDest) {
		this.latDest = latDest;
	}
	public float getLonDest() {
		return lonDest;
	}
	public void setLonDest(float lonDest) {
		this.lonDest = lonDest;
	}
	private String adresse;
	private String adresseDest ;
	private float lat;
	private float lon;
	private float latDest;
	private float lonDest;
	
	
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getAdresseDest() {
		return adresseDest;
	}
	public void setAdresseDest(String adresseDest) {
		this.adresseDest = adresseDest;
	}
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLon() {
		return lon;
	}
	public void setLon(float lon) {
		this.lon = lon;
	}
	
}
