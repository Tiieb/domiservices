package fr.univ.service.personne.services;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.univ.service.personne.dao.TrajetDao;
import fr.univ.service.personne.dao.UserDao;
import fr.univ.service.personne.form.AjouterTrajetForm;
import fr.univ.service.personne.form.RechercheTrajetForm;
import fr.univ.service.personne.form.RecupererTousLesTrajetsForm;
import fr.univ.service.personne.model.TrajetEntity;
import fr.univ.service.personne.test.Constante;
import fr.univ.service.personne.test.ConvertisseurDate;



@Repository
public class TrajetService {
	@Autowired
	TrajetDao mTrajetDao;
	@Autowired
	UserDao mUserDao;
	
	public boolean ajouterTrajet(AjouterTrajetForm pAjouterTrajetForm){
		
		
	 TrajetEntity vTrajetEntity = new TrajetEntity();
	 vTrajetEntity.setIdUser(pAjouterTrajetForm.getIdUtilisateur());
	 vTrajetEntity.setVilleDepart(pAjouterTrajetForm.getVilleDepart());
	 vTrajetEntity.setVilleDestination(pAjouterTrajetForm.getVilleDestination());
	 vTrajetEntity.setVilleIntermediaire(pAjouterTrajetForm.getVilleIntermediaire());
	 vTrajetEntity.setDateDepart(ConvertisseurDate.stringToDate(pAjouterTrajetForm.getDateDepart(), Constante.DATE_FORMAT_1));
	 TrajetEntity retourTrajetEntity = new TrajetEntity(); 
	 System.out.println(pAjouterTrajetForm.getIdUtilisateur());
	 retourTrajetEntity=mTrajetDao.create(vTrajetEntity);
	 if(null != retourTrajetEntity){
	return true;
		
	}else
	{
		return false;
	}
	 }
	
	
	
	public List<TrajetEntity> rechercherTrajet(RechercheTrajetForm pRechercheTrajetForm){
		
		Date date= ConvertisseurDate.stringToDate(pRechercheTrajetForm.getDateDepartRecherche(), Constante.DATE_FORMAT_1);
		
		List<TrajetEntity> vListTrajet = mTrajetDao.getTrajetRecherche(pRechercheTrajetForm.getVilleDepartRecherche(),pRechercheTrajetForm.getVilleDestinationRecherche(),date);
		
		return vListTrajet;
		
		
		 
			
		}
	
public List<TrajetEntity> getTousTrajet(int pIdUser){
		
		
		
		List<TrajetEntity> vListTrajet = mTrajetDao.getTrajets(pIdUser);
		if(null!= vListTrajet){
			
			System.out.println("liste n'est pas vide");
		}else{
			System.out.println("lise vide");
		}
		
		return vListTrajet;
		
			
		}
public boolean supprimerTrajet(int pIdTrajet){
	
	
	mTrajetDao.delete(pIdTrajet);
		
		return true;
	
}
	
	
	
	
	
	

	public TrajetDao getmTrajetDao() {
		return mTrajetDao;
	}

	public void setmTrajetDao(TrajetDao mTrajetDao) {
		this.mTrajetDao = mTrajetDao;
	}
	
	

}
