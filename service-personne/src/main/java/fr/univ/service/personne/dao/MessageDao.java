package fr.univ.service.personne.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.metamodel.Metamodel;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.TrajetEntity;
import fr.univ.service.personne.model.MessageEntity;
import fr.univ.service.personne.model.MessagePersoEntity;
import fr.univ.service.personne.form.MessageBean;

@Repository
public class MessageDao {

	@PersistenceContext
	EntityManager em = new EntityManager() {
		
		public <T> T unwrap(Class<T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void setProperty(String arg0, Object arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void setFlushMode(FlushModeType arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public void remove(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0, LockModeType arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0, Map<String, Object> arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public void persist(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public <T> T merge(T arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void lock(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
			// TODO Auto-generated method stub
			
		}
		
		public void lock(Object arg0, LockModeType arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void joinTransaction() {
			// TODO Auto-generated method stub
			
		}
		
		public boolean isOpen() {
			// TODO Auto-generated method stub
			return false;
		}
		
		public EntityTransaction getTransaction() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T getReference(Class<T> arg0, Object arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Map<String, Object> getProperties() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Metamodel getMetamodel() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public LockModeType getLockMode(Object arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public FlushModeType getFlushMode() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public EntityManagerFactory getEntityManagerFactory() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Object getDelegate() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public CriteriaBuilder getCriteriaBuilder() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void flush() {
			// TODO Auto-generated method stub
			
		}
		
		public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2, Map<String, Object> arg3) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T find(Class<T> arg0, Object arg1, Map<String, Object> arg2) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T find(Class<T> arg0, Object arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void detach(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public <T> TypedQuery<T> createQuery(String arg0, Class<T> arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createQuery(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNativeQuery(String arg0, String arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNativeQuery(String arg0, Class arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNativeQuery(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> TypedQuery<T> createNamedQuery(String arg0, Class<T> arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNamedQuery(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public void close() {
			// TODO Auto-generated method stub
			
		}
		
		public void clear() {
			// TODO Auto-generated method stub
			
		}
	};

	public MessageDao() {
		// TODO Auto-generated constructor stub
	}
	
	public MessageBean getMesMessages(int pIdUser) {
		
		MessageBean myMessage = new MessageBean() ;
		
		List<MessageEntity> message_send = new ArrayList<MessageEntity>();
		List<MessageEntity> message_rec = new ArrayList<MessageEntity>();
		List<MessagePersoEntity> message_perso_send = new ArrayList<MessagePersoEntity>();
		List<MessagePersoEntity> message_perso_rec = new ArrayList<MessagePersoEntity>();

		Query vQuery = em.createQuery("SELECT U FROM MessageEntity U WHERE U.idUserSend = :idUser");
		vQuery.setParameter("idUser", pIdUser);
		message_send = (List<MessageEntity>) vQuery.getResultList();
		
		Query vQuery1 = em.createQuery("SELECT U FROM MessageEntity U WHERE U.idUserRec = :idUser");
		vQuery1.setParameter("idUser", pIdUser);
		message_rec = (List<MessageEntity>) vQuery1.getResultList();
		
		Query vQuery2 = em.createQuery("SELECT U FROM MessagePersoEntity U WHERE U.idUserSend = :idUser");
		vQuery2.setParameter("idUser", pIdUser);
		message_perso_send = (List<MessagePersoEntity>) vQuery2.getResultList();
		
		Query vQuery3 = em.createQuery("SELECT U FROM MessagePersoEntity U WHERE U.idUserRec = :idUser");
		vQuery3.setParameter("idUser", pIdUser);
		message_perso_rec = (List<MessagePersoEntity>) vQuery3.getResultList();
		
		myMessage.setMessage_send(message_send);
		myMessage.setMessage_rec(message_rec);
		myMessage.setMessage_perso_send(message_perso_send);
		myMessage.setMessage_perso_rec(message_perso_rec);
		
		
		
		return myMessage;

	}

	@Transactional
	public MessageEntity create(final MessageEntity pMessageEntity) {
		em.persist(pMessageEntity);
		return pMessageEntity;
	}
	
	@Transactional
	public MessagePersoEntity create(final MessagePersoEntity pMessageEntity) {
		em.persist(pMessageEntity);
		return pMessageEntity;
	}
	
	
	@Transactional
	public boolean setEtat(int idDemmande, String etat) {

		DemmandeEntity dem = em.find(DemmandeEntity.class, idDemmande) ;
		dem.setEtat(etat);
		em.merge(dem);
		
		return true;

	}

	@Transactional
	public void delete(final Object id) {
		this.em.remove(em.getReference(DemmandeEntity.class, id));
	}
	
	@Transactional
	public void update(final DemmandeEntity com) {
		this.em.merge(com) ;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

}
