package fr.univ.service.personne.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.univ.service.personne.form.AjouterCommandeForm;
import fr.univ.service.personne.form.DemmandeUser;
import fr.univ.service.personne.form.MessageForm;
import fr.univ.service.personne.form.RechercheTrajetForm;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;
import fr.univ.service.personne.services.CommandeService;
import fr.univ.service.personne.services.DemmandeService;
import fr.univ.service.personne.services.UtilisateurService;

@Controller
@Scope("session")
@SessionAttributes(value = "utilisateurSession", types = { UtilisateurEntity.class })
public class CommandeController {

	@Autowired
	CommandeService mCommandeService;
	@Autowired
	DemmandeService mDemmandeService;
	@Autowired
	UtilisateurService mUtilisateurService;

	@RequestMapping(value = "**/AjouterCommandeAffichage", method = RequestMethod.GET)
	public ModelAndView ajouterCommandeAffichage(
			@ModelAttribute("ajouterCommandeForm") AjouterCommandeForm pAjouterCommandeForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap pRedirectMapAttribut, ModelAndView pModelAndView) {

		if (pRedirectMapAttribut.containsKey("CommandeAjouter")) {
			System.out.println(pRedirectMapAttribut.get("CommandeAjouter"));
			pModelAndView.getModelMap().addAttribute("CommandeAjouter",
					(String) pRedirectMapAttribut.get("CommandeAjouter"));

		}
		if (pRedirectMapAttribut.containsKey("ErreurAjoutCommande")) {

			pModelAndView.getModelMap().addAttribute("ErreurAjoutCommande",
					(String) pRedirectMapAttribut.get("ErreurAjoutCommande"));

		}
		pModelAndView.setViewName("ajouterCommande");
		return pModelAndView;

	}

	@RequestMapping(value = "**/MesCommandes", method = RequestMethod.GET)
	public ModelAndView recupererToutesMesCommandesRun(HttpServletRequest request,
			@ModelAttribute("ajouterCommandeForm") AjouterCommandeForm pAjouterCommandeForm,
			@ModelAttribute("MessageForm") MessageForm MessageForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap pRedirectMapAttribut, 
			ModelAndView pModelAndView) {
		
		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		List<CommandeEntity> vListeDesCommandes = mCommandeService.getTousCommande(user.getIdUser());
		List<List<DemmandeEntity>> vListesDesDemmandes = new ArrayList<List<DemmandeEntity>>();
		List<List<UtilisateurEntity>> vListesDesUtilisateur = new ArrayList<List<UtilisateurEntity>>();
		
		
		if (!vListeDesCommandes.isEmpty()) {
			
			for (int i = 0; i < vListeDesCommandes.size(); i++) {

				DemmandeUser vListeDesDemmandesUser = mDemmandeService
						.getDemmandesPerCommande(vListeDesCommandes.get(i).getIdCommande());
				
				vListesDesDemmandes.add(vListeDesDemmandesUser.getDemmandeList());
				vListesDesUtilisateur.add(vListeDesDemmandesUser.getUserList());

			}
			boolean test=false;
			for (List<DemmandeEntity> dem : vListesDesDemmandes){
				for (DemmandeEntity demm : dem){
					if(demm.getEtat().equals("En cours")){
						test=true ;
					}
				}
			}
			pModelAndView.getModelMap().addAttribute("test", test);
			pModelAndView.getModelMap().addAttribute("listeDesCommandes", vListeDesCommandes);

			if (!vListesDesDemmandes.isEmpty()) {
				pModelAndView.getModelMap().addAttribute("listesDesDemmande", vListesDesDemmandes);
				pModelAndView.getModelMap().addAttribute("listeDesUtilisateurs", vListesDesUtilisateur);
				System.out.println("contient");
			}else {
				pModelAndView.getModelMap().addAttribute("demmandeVide", "Il n'y a aucune demande pour cette commande");
				System.out.println("ne contient pas");
			}
			System.out.println((String) pModelAndView.getModelMap().get("demmandeVide"));
			if(pModelAndView.getModelMap().containsAttribute("listesDesDemmande"))
				System.out.println("ttttt");
		} else {
			pModelAndView.getModelMap().addAttribute("listeVide", "Vous n'avez aucune commande");
		}

		if (pRedirectMapAttribut.containsKey("CommandeAjout�")) {
			pModelAndView.getModelMap().addAttribute("CommandeAjout�", pRedirectMapAttribut.get("CommandeAjout�"));
		}

		if (pRedirectMapAttribut.containsKey("messageInfo")) {
			pModelAndView.getModelMap().addAttribute("messageInfo", pRedirectMapAttribut.get("messageInfo"));
		}

		pModelAndView.setViewName("mesCommandes");
		return pModelAndView;
	}

	@RequestMapping(value = "**/AjouterCommandeRun", method = RequestMethod.GET)
	public ModelAndView ajouterCommandeRun(
			@ModelAttribute("ajouterCommandeForm") AjouterCommandeForm pAjouterCommandeForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, 
			ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		int id = pAjouterCommandeForm.getIdUser();

		if (mCommandeService.ajouterCommande(pAjouterCommandeForm)) {

			vRedirectMap.put("CommandeAjout�", "La commande a �t� enregistr� avec succ�s");
			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:MesCommandes");
			return pModelAndView;

		} else {

			vRedirectMap.put("ErreurAjoutCommande",
					"une erreur de serveur s'est produite. votre trajet n'a pas �t� enregistr� ");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:AjouterCommandeDashboard");
			return pModelAndView;

		}

	}

	@RequestMapping(value = "**/supprimerCommande", method = RequestMethod.GET)
	public ModelAndView supprimerCommande(@RequestParam("idCommande") int idCommande,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();

		mCommandeService.supprimerCommande(idCommande);

		vRedirectMap.put("messageInfo", "Le trajet a �t� supprim�");
		pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
		pModelAndView.setViewName("redirect:MesCommandes");

		return pModelAndView;
		// else {
		//
		// vRedirectMap.put("messageErreur", "Le trajet n'a pas �t� supprim�");
		//
		// pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut",
		// vRedirectMap);
		// pModelAndView.setViewName("redirect:recupererTousLesTrajetAffichage");

		// return pModelAndView;
		// }
	}

	@RequestMapping(value = "**/modifierCommande", method = RequestMethod.GET)
	public ModelAndView modifierCommande(@RequestParam("idCommande") int idCommande,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();

		mCommandeService.supprimerCommande(idCommande);

		vRedirectMap.put("messageInfo", "Le trajet a �t� supprim�");
		pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
		pModelAndView.setViewName("redirect:MesCommandes");

		return pModelAndView;
		// else {
		//
		// vRedirectMap.put("messageErreur", "Le trajet n'a pas �t� supprim�");
		//
		// pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut",
		// vRedirectMap);
		// pModelAndView.setViewName("redirect:recupererTousLesTrajetAffichage");

		// return pModelAndView;
		// }
	}

}
