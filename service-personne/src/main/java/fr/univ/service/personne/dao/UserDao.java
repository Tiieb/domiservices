package fr.univ.service.personne.dao;
/**
 * 
 */

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;

/**
 * @author personnel
 *
 */
@Repository
public class UserDao {

	@PersistenceContext
	EntityManager em;

	/**
	 * 
	 */
	public UserDao() {
		// TODO Auto-generated constructor stub
	}

	@Transactional
	public UtilisateurEntity create(final UtilisateurEntity pUtilisateurEntity) {
		em.persist(pUtilisateurEntity);
		return pUtilisateurEntity;
	}

	@Transactional
	public void delete(final Object id) {
		this.em.remove(em.getReference(UtilisateurEntity.class, id));
	}

	public UtilisateurEntity find(final int id) {
		return (UtilisateurEntity) this.em.find(UtilisateurEntity.class, id);
	}

	@Transactional
	public UtilisateurEntity update(final UtilisateurEntity pUtilisateurEntity) {
		return this.em.merge(pUtilisateurEntity);
	}

	@Transactional
	public UtilisateurEntity getUtilisateur(int idUser) {

		Query vQuery = em.createQuery("SELECT U FROM UtilisateurEntity U WHERE U.idUser = :idUser ");
		vQuery.setParameter("idUser", idUser);

		UtilisateurEntity vUser = null;
		try {
			vUser = (UtilisateurEntity) vQuery.getSingleResult();
			if (null != vUser) {
				em.refresh(vUser);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("erreur pas user retourn�");
		}
		return vUser;
	}

	@Transactional
	public UtilisateurEntity getUtilisateurConnexion(String pEmail, String pPassword) {

		Query vQuery = em.createQuery(
				"SELECT U FROM UtilisateurEntity U WHERE U.emailUtilisateur = :emailUtilisateur and U.passwordUtilisateur = :passwordUtilisateur ");
		vQuery.setParameter("emailUtilisateur", pEmail);
		vQuery.setParameter("passwordUtilisateur", pPassword);

		UtilisateurEntity vUser = null;
		try {
			vUser = (UtilisateurEntity) vQuery.getSingleResult();
			if (null != vUser) {
				em.refresh(vUser);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("erreur pas de centre de frais retourn�");
		}

		return vUser;

	}
	
	public List<UtilisateurEntity> getUserPerDemmande(int idDemmande) {

		Query vQuery = em.createQuery("SELECT U FROM UtilisateurEntity U JOIN DemmandeEntity J WHERE ( J.idDemmande = :idDemmande AND U.idUser = J.idUser )");
		vQuery.setParameter("idDemmande", idDemmande);
		
		return (List<UtilisateurEntity>) vQuery.getResultList();

	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

}
