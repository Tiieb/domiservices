package fr.univ.service.personne.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ServerSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionAttributeStore;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.SessionScope;
import org.springframework.web.method.annotation.SessionAttributesHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.univ.service.personne.form.ConnexionForm;
import fr.univ.service.personne.form.MessageForm;
import fr.univ.service.personne.form.RechercheTrajetForm;
import fr.univ.service.personne.form.UtilisateurForm;
import fr.univ.service.personne.form.AdresseBean;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;
import fr.univ.service.personne.services.CommandeService;
import fr.univ.service.personne.services.TrajetService;
import fr.univ.service.personne.services.UtilisateurService;
import fr.univ.service.personne.services.MapService;

@Controller
@Scope("session")
@SessionAttributes(value = "utilisateurSession", types = { UtilisateurEntity.class })
public class DefaultController {

	@Autowired
	UtilisateurService utilisateurService;
	@Autowired
	CommandeService mCommandeService;
	// controleur qui affiche la jsp home_page
	MapService map = new MapService();

	@RequestMapping(value = "**/HomePage", method = RequestMethod.GET)
	public ModelAndView redirectHomePage(@ModelAttribute("utilisateurForm") UtilisateurForm pUtilisateurForm,
			@ModelAttribute("connexionForm") ConnexionForm pConnexionForm,
			@ModelAttribute("redirectMapAttribut") HashMap<String, Object> pRedirectMapAttribut,
			ModelAndView pModelAndView) {

		if (pRedirectMapAttribut.containsKey("messageErreur")) {

			pModelAndView.getModelMap().addAttribute("messageErreur",
					(String) pRedirectMapAttribut.get("messageErreur"));

		}
		if (pRedirectMapAttribut.containsKey("utilisateurAjouter")) {

			pModelAndView.getModelMap().addAttribute("utilisateurAjouter",
					(String) pRedirectMapAttribut.get("utilisateurAjouter"));

		}

		if (null != pRedirectMapAttribut) {

			if (pRedirectMapAttribut.containsKey("messageInfo")) {
				pModelAndView.getModelMap().addAttribute("messageInfo",
						(String) pRedirectMapAttribut.get("messageInfo"));
			}
		}
		pModelAndView.setViewName("home_page");
		return pModelAndView;
	}

	// controleur qui fait l'ajout d'un utilisateur

	@RequestMapping(value = "**/ajouterUtilisateurRun", method = RequestMethod.GET)
	public ModelAndView ajouterUtilisateurRun(
			@ModelAttribute("utilisateurForm") UtilisateurForm pUtilisateurForm,
			@ModelAttribute("connexionForm") ConnexionForm pConnexionForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, 
			ModelAndView pModelAndView) {

		if (utilisateurService.addUser(pUtilisateurForm)) {
			Map<String, Object> vRedirectMap = new HashMap<String, Object>();

			vRedirectMap.put("utilisateurAjouter", "Inscription effectu�e");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:HomePage");
			return pModelAndView;

		} else {
			pModelAndView.setViewName("home_page");
			return pModelAndView;

		}

	}

	// controlleur d'authentification
	@RequestMapping(value = "**/connexionUtilisateurRun", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView connexionUtilisateur(HttpServletRequest request,
			@ModelAttribute("connexionForm") ConnexionForm pConnexionForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		UtilisateurEntity vUser = utilisateurService.getConnexionUtilisateur(pConnexionForm);

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();

		if (null != vUser) {

			request.getSession().setAttribute("utilisateurSession", vUser);
			vRedirectMap.put("utilisateurSession", vUser);
			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:espacePersoDisplay");

			return pModelAndView;

		} else {

			vRedirectMap.put("messageErreur", "L'authentification a �chou�e, veuillez r�essayer ");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);

			pModelAndView.setViewName("redirect:HomePage");
			return pModelAndView;
		}

	}

	// controlleur qui permet d'acceder � l'espace perso de l'utilisateur

	@RequestMapping(value = "**/espacePersoDisplay", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView espacePersoDisplay(HttpServletRequest request,
			@ModelAttribute("connexionForm") ConnexionForm pConnexionForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap<String, Object> pRedirectMapAttribut,
			ModelAndView pModelAndView)
			throws JsonParseException, JsonMappingException, MalformedURLException, IOException {

		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		List<UtilisateurEntity> Users = new ArrayList<UtilisateurEntity>();
		List<CommandeEntity> vListeDesCommandes = mCommandeService.getCommandesProche(user.getIdUser());
		List<AdresseBean> coords = new ArrayList<AdresseBean>();
		
		for (CommandeEntity commande : vListeDesCommandes) {
			Users.add(utilisateurService.getUser(commande.getIdUser()));
		}

		if (!vListeDesCommandes.isEmpty() && !Users.isEmpty()) {

			pModelAndView.getModelMap().addAttribute("ListeCommandesProche", vListeDesCommandes);
			pModelAndView.getModelMap().addAttribute("Users", Users);

			for (CommandeEntity commande : vListeDesCommandes) {
				coords.add(map.convertAdresse(commande.getPositionColis(), commande.getDestinationColis())) ;
				
			}
			
			pModelAndView.getModelMap().addAttribute("coords", coords);

		} else {
			pModelAndView.getModelMap().addAttribute("aucuneCommande", "il n'y a aucune commande pour le moment");
		}
		if (pRedirectMapAttribut.containsKey("DemmandeAjout�")) {
			pModelAndView.getModelMap().addAttribute("DemmandeAjout�",
					(String) pRedirectMapAttribut.get("DemmandeAjout�"));

		}
		if (pRedirectMapAttribut.containsKey("ErreurAjoutDemmande")) {

			pModelAndView.getModelMap().addAttribute("ErreurAjoutDemmande",
					(String) pRedirectMapAttribut.get("ErreurAjoutDemmande"));

		}
		pModelAndView.setViewName("dashboard");
		return pModelAndView;

	}
	
	
	@RequestMapping(value = "**/ModifierUser", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView ModifierUser(
			HttpSession status,
			HttpServletRequest request,
			@ModelAttribute("UtilisateurForm") UtilisateurForm UtilisateurForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap,
			ModelAndView pModelAndView)
			throws JsonParseException, JsonMappingException, MalformedURLException, IOException {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		
		
		if (utilisateurService.modifier(user.getIdUser(), UtilisateurForm)) {
			pRedirectAttributesModelMap.addFlashAttribute("UtilisateurModifie", "Votre profil a bien �t� modifi�, veuillez vous deconnecter pour que les changements prennent effet");
			
			
		} else {
			pModelAndView.getModelMap().addAttribute("aucuneCommande", "il n'y a aucune commande pour le moment");
		}

		pModelAndView.setViewName("redirect:profile");
		return pModelAndView;

	}
	
	@RequestMapping(value = "**/profile", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView profile(
			HttpServletRequest request,
			@ModelAttribute("UtilisateurForm") UtilisateurForm pUtilisateurForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap<String, Object> pRedirectMapAttribut,
			ModelAndView pModelAndView) {

		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		
		pModelAndView.getModelMap().addAttribute("user", user);
		
		if (pRedirectMapAttribut.containsKey("UtilisateurModifie")) {
			pModelAndView.getModelMap().addAttribute("UtilisateurModifie", (String) pRedirectMapAttribut.get("UtilisateurModifie"));

		}
		pModelAndView.setViewName("profile");
		return pModelAndView;
	}
	
	@RequestMapping(value = "**/profile_view", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView profile_view(HttpServletRequest request,
			@RequestParam("idUser") int idUser,
			@ModelAttribute("MessageForm") MessageForm MessageForm,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap<String, Object> pRedirectMapAttribut,
			ModelAndView pModelAndView) {

		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		UtilisateurEntity user_view = utilisateurService.getUser(idUser);
		List<CommandeEntity> user_commands = mCommandeService.getTousCommande(idUser);
		
		pModelAndView.getModelMap().addAttribute("user", user);
		pModelAndView.getModelMap().addAttribute("user_view", user_view);
		pModelAndView.getModelMap().addAttribute("user_commandes", user_commands);

		pModelAndView.setViewName("profile_view");
		return pModelAndView;
	}

	@RequestMapping(value = "**/deconnect", method = { RequestMethod.GET, RequestMethod.POST })
	public ModelAndView deconnect(SessionStatus status, 
			ModelAndView pModelAndView,
			RedirectAttributesModelMap pRedirectAttributesModelMap) {

		status.setComplete();
		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		vRedirectMap.put("messageInfo", "Merci pour votre visite!!");
		pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);

		pModelAndView.setViewName("redirect:HomePage");

		return pModelAndView;
	}

	public UtilisateurService getUtilisateurService() {
		return utilisateurService;
	}

	public void setUtilisateurService(UtilisateurService utilisateurService) {
		this.utilisateurService = utilisateurService;
	}

}