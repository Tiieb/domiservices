package fr.univ.service.personne.services;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import fr.univ.service.personne.dao.CommandeDao;
import fr.univ.service.personne.form.AjouterCommandeForm;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.TrajetEntity;

@Repository
public class CommandeService {

	@Autowired
	CommandeDao mCommandeDao;

	public boolean ajouterCommande(AjouterCommandeForm pAjouterCommandeForm) {

		CommandeEntity vCommandeEntity = new CommandeEntity();

		vCommandeEntity.setIdCommande(pAjouterCommandeForm.getIdCommande());
		vCommandeEntity.setTypeCommande(pAjouterCommandeForm.getTypeCommande());
		vCommandeEntity.setPositionColis(pAjouterCommandeForm.getPositionColis());
		vCommandeEntity.setDestinationColis(pAjouterCommandeForm.getDestinationColis());
		vCommandeEntity.setIdUser(pAjouterCommandeForm.getIdUser());

		CommandeEntity retourCommandeEntity = new CommandeEntity();

		retourCommandeEntity = mCommandeDao.create(vCommandeEntity);
		if (null != retourCommandeEntity) {
			return true;

		} else {
			return false;
		}
	}

	public List<CommandeEntity> getTousCommande(int pIdUser) {

		List<CommandeEntity> vListCommande = mCommandeDao.getCommande(pIdUser);
		if (null != vListCommande) {

			System.out.println("liste commandes perso n'est pas vide");
		} else {
			System.out.println("liste commandes perso vide");
		}
		
		Collections.reverse(vListCommande);

		return vListCommande;

	}
	
	public List<CommandeEntity> getCommandesProche(int pIdUser) {
		
		List<CommandeEntity> vListCommande = mCommandeDao.getCommandeProche(pIdUser);
		
		
		if (null != vListCommande) {
			System.out.println("liste commandes proches n'est pas vide");
		} else {
			System.out.println("liste commande proches vide");
		}
		
		Collections.reverse(vListCommande);

		return vListCommande;

	}
	
	public void supprimerCommande(int id){
		mCommandeDao.delete(id);
	}
	
	public void modifierCommande(int id, CommandeEntity commande ){
		CommandeEntity com = mCommandeDao.getCommandeById(id) ;
		com.setIdCommande(commande.getIdCommande());
		com.setDestinationColis(commande.getDestinationColis());
		com.setIdUser(commande.getIdUser());
		com.setPositionColis(commande.getPositionColis());
		mCommandeDao.update(com);
	}

}
