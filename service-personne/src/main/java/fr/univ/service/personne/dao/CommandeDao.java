package fr.univ.service.personne.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.FlushModeType;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.metamodel.Metamodel;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.TrajetEntity;

@Repository
public class CommandeDao {

	@PersistenceContext
	EntityManager em = new EntityManager() {
		
		public <T> T unwrap(Class<T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void setProperty(String arg0, Object arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void setFlushMode(FlushModeType arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public void remove(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0, LockModeType arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0, Map<String, Object> arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void refresh(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public void persist(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public <T> T merge(T arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void lock(Object arg0, LockModeType arg1, Map<String, Object> arg2) {
			// TODO Auto-generated method stub
			
		}
		
		public void lock(Object arg0, LockModeType arg1) {
			// TODO Auto-generated method stub
			
		}
		
		public void joinTransaction() {
			// TODO Auto-generated method stub
			
		}
		
		public boolean isOpen() {
			// TODO Auto-generated method stub
			return false;
		}
		
		public EntityTransaction getTransaction() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T getReference(Class<T> arg0, Object arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Map<String, Object> getProperties() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Metamodel getMetamodel() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public LockModeType getLockMode(Object arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public FlushModeType getFlushMode() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public EntityManagerFactory getEntityManagerFactory() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Object getDelegate() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public CriteriaBuilder getCriteriaBuilder() {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void flush() {
			// TODO Auto-generated method stub
			
		}
		
		public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2, Map<String, Object> arg3) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T find(Class<T> arg0, Object arg1, LockModeType arg2) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T find(Class<T> arg0, Object arg1, Map<String, Object> arg2) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> T find(Class<T> arg0, Object arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public void detach(Object arg0) {
			// TODO Auto-generated method stub
			
		}
		
		public <T> TypedQuery<T> createQuery(String arg0, Class<T> arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createQuery(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNativeQuery(String arg0, String arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNativeQuery(String arg0, Class arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNativeQuery(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public <T> TypedQuery<T> createNamedQuery(String arg0, Class<T> arg1) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public Query createNamedQuery(String arg0) {
			// TODO Auto-generated method stub
			return null;
		}
		
		public boolean contains(Object arg0) {
			// TODO Auto-generated method stub
			return false;
		}
		
		public void close() {
			// TODO Auto-generated method stub
			
		}
		
		public void clear() {
			// TODO Auto-generated method stub
			
		}
	};

	public CommandeDao() {
		// TODO Auto-generated constructor stub
	}

	@Transactional
	public CommandeEntity create(final CommandeEntity pCommandeEntity) {
		em.persist(pCommandeEntity);
		return pCommandeEntity;
	}

	public List<CommandeEntity> getCommande(int pIdUser) {

		Query vQuery = em.createQuery("SELECT U FROM CommandeEntity U WHERE U.idUser = :idUser");
		vQuery.setParameter("idUser", pIdUser);

		return (List<CommandeEntity>) vQuery.getResultList();

	}
	
	public CommandeEntity getCommandeById(int pIdUser) {
		return em.find(CommandeEntity.class, pIdUser);
	}
	
	public List<CommandeEntity> getCommandeProche(int pIdUser) {

		Query vQuery = em.createQuery("SELECT U FROM CommandeEntity U WHERE U.idUser != :idUser");
		vQuery.setParameter("idUser", pIdUser);

		return (List<CommandeEntity>) vQuery.getResultList();

	}

	@Transactional
	public void delete(final Object id) {
		this.em.remove(em.getReference(CommandeEntity.class, id));
	}
	
	@Transactional
	public void update(final CommandeEntity com) {
		this.em.merge(com) ;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

}
