package fr.univ.service.personne.form;

import java.util.List;

import fr.univ.service.personne.model.MessageEntity;
import fr.univ.service.personne.model.MessagePersoEntity;

public class MessageBean {
	

	public List<MessagePersoEntity> getMessage_perso_send() {
		return message_perso_send;
	}
	public void setMessage_perso_send(List<MessagePersoEntity> message_perso_send) {
		this.message_perso_send = message_perso_send;
	}
	public List<MessagePersoEntity> getMessage_perso_rec() {
		return message_perso_rec;
	}
	public void setMessage_perso_rec(List<MessagePersoEntity> message_perso_rec) {
		this.message_perso_rec = message_perso_rec;
	}
	private List<MessageEntity> message_send ;
	private List<MessageEntity> message_rec ;
	private List<MessagePersoEntity> message_perso_send;
	private List<MessagePersoEntity> message_perso_rec;
	
	public List<MessageEntity> getMessage_send() {
		return message_send;
	}
	public void setMessage_send(List<MessageEntity> message_send) {
		this.message_send = message_send;
	}
	public List<MessageEntity> getMessage_rec() {
		return message_rec;
	}
	public void setMessage_rec(List<MessageEntity> message_rec) {
		this.message_rec = message_rec;
	}
	
	
	
}
