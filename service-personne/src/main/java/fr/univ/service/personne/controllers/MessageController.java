package fr.univ.service.personne.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.SessionScope;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.univ.service.personne.form.AjouterCommandeForm;
import fr.univ.service.personne.form.DemmandeUser;
import fr.univ.service.personne.form.RechercheTrajetForm;
import fr.univ.service.personne.form.MessageForm;
import fr.univ.service.personne.form.MessageBean;
import fr.univ.service.personne.model.CommandeEntity;
import fr.univ.service.personne.model.DemmandeEntity;
import fr.univ.service.personne.model.UtilisateurEntity;
import fr.univ.service.personne.services.CommandeService;
import fr.univ.service.personne.services.DemmandeService;
import fr.univ.service.personne.services.UtilisateurService;
import fr.univ.service.personne.services.MessageService;

@Controller
@Scope("session")
@SessionAttributes(value = "utilisateurSession", types = { UtilisateurEntity.class })
public class MessageController {

	@Autowired
	CommandeService mCommandeService;
	@Autowired
	MessageService mMessageService;
	@Autowired
	DemmandeService mDemmandeService;
	@Autowired
	UtilisateurService mUtilisateurService;

	@RequestMapping(value = "**/messagerie", method = RequestMethod.GET)
	public ModelAndView recupererToutesMesMessagesPersoRun(HttpServletRequest request,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap pRedirectMapAttribut, ModelAndView pModelAndView) {

		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		MessageBean vListeDesMessage = mMessageService.getMesMessages(user.getIdUser());
		List<UtilisateurEntity> vListeUtilisateurs_perso_send = new ArrayList<UtilisateurEntity>();
		List<UtilisateurEntity> vListeUtilisateurs_perso_rec = new ArrayList<UtilisateurEntity>();
		
		for(int i=0;i<vListeDesMessage.getMessage_perso_send().size();i++){
			System.out.println("msgpersosend : 111"+vListeDesMessage.getMessage_perso_send().get(i).getIdMessage());
		}
		for(int i=0;i<vListeDesMessage.getMessage_perso_rec().size();i++){
			System.out.println("msgpersorec 111: "+vListeDesMessage.getMessage_perso_rec().get(i).getIdMessage());
		}
		for(int i=0;i<vListeDesMessage.getMessage_send().size();i++){
			System.out.println("msgsend : 111"+vListeDesMessage.getMessage_send().get(i).getIdMessage());
		}
		for(int i=0;i<vListeDesMessage.getMessage_rec().size();i++){
			System.out.println("msgrec : 111"+vListeDesMessage.getMessage_rec().get(i).getIdMessage());
		}

		if (!vListeDesMessage.getMessage_perso_send().isEmpty()) {

			for (int i = 0; i < vListeDesMessage.getMessage_perso_send().size(); i++) {
				if (vListeDesMessage.getMessage_perso_send().get(i) != null) {
					UtilisateurEntity utilisateur_perso_send = mUtilisateurService
							.getUser(vListeDesMessage.getMessage_perso_send().get(i).getIdUserRec());
					vListeUtilisateurs_perso_send.add(utilisateur_perso_send);
//					System.out.println("--------------------------------------");
//					System.out.println(vListeUtilisateurs_perso_send.get(i).getNomUser());
//					System.out.println(vListeDesMessage.getMessage_perso_send().get(i).getMesage());
//					System.out.println("--------------------------------------");
				}
			}
			pModelAndView.getModelMap().addAttribute("message_perso_send", vListeDesMessage.getMessage_perso_send());
			pModelAndView.getModelMap().addAttribute("user_perso_send", vListeUtilisateurs_perso_send);
		}
		
		if (!vListeDesMessage.getMessage_perso_rec().isEmpty()) {

			for (int i = 0; i < vListeDesMessage.getMessage_perso_rec().size(); i++) {
				if (vListeDesMessage.getMessage_perso_rec().get(i) != null) {
					UtilisateurEntity utilisateur_perso_rec = mUtilisateurService
							.getUser(vListeDesMessage.getMessage_perso_rec().get(i).getIdUserSend());
					vListeUtilisateurs_perso_rec.add(utilisateur_perso_rec);
//					System.out.println("--------------------------------------");
//					System.out.println(vListeUtilisateurs_perso_rec.get(i).getNomUser());
//					System.out.println(vListeDesMessage.getMessage_rec().get(i).getMesage());
//					System.out.println("--------------------------------------");

				}
			}
			pModelAndView.getModelMap().addAttribute("message_perso_rec", vListeDesMessage.getMessage_perso_rec());
			pModelAndView.getModelMap().addAttribute("user_perso_rec", vListeUtilisateurs_perso_rec);
		}
//			if (!vListeUtilisateurs_perso_send.isEmpty()) {
//				for (int i = 0; i < vListeDesMessage.getMessage_send().size(); i++) {
//					System.out.println(vListeUtilisateurs_perso_send.get(i).getNomUser());
//				}
//			}

			

			

		

		pModelAndView.setViewName("messagerie");
		return pModelAndView;
	}

	@RequestMapping(value = "**/messagerieDemmande", method = RequestMethod.GET)
	public ModelAndView recupererToutesMesMessagesRun(HttpServletRequest request,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("redirectMapAttribut") HashMap pRedirectMapAttribut, ModelAndView pModelAndView) {

		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");
		MessageBean vListeDesMessage = mMessageService.getMesMessages(user.getIdUser());
		List<UtilisateurEntity> vListeUtilisateurs_send = new ArrayList<UtilisateurEntity>();
		List<UtilisateurEntity> vListeUtilisateurs_rec = new ArrayList<UtilisateurEntity>();

		if (!vListeDesMessage.getMessage_send().isEmpty()) {

			for (int i = 0; i < vListeDesMessage.getMessage_send().size(); i++) {
				if (vListeDesMessage.getMessage_send().get(i) != null) {
					UtilisateurEntity utilisateur_send = mUtilisateurService
							.getUser(vListeDesMessage.getMessage_send().get(i).getIdUserRec());
					vListeUtilisateurs_send.add(utilisateur_send);
//					System.out.println("--------------------------------------");
//					System.out.println(vListeUtilisateurs_send.get(i).getNomUser());
//					System.out.println(vListeDesMessage.getMessage_send().get(i).getMesage());
//					System.out.println("--------------------------------------");

				}
			}
		}

		if (!vListeDesMessage.getMessage_rec().isEmpty()) {
			for (int i = 0; i < vListeDesMessage.getMessage_rec().size(); i++) {
				if (vListeDesMessage.getMessage_rec().get(i) != null) {
					UtilisateurEntity utilisateur_rec = mUtilisateurService
							.getUser(vListeDesMessage.getMessage_rec().get(i).getIdUserSend());
					vListeUtilisateurs_rec.add(utilisateur_rec);
//					System.out.println("--------------------------------------");
//					System.out.println(vListeUtilisateurs_rec.get(i).getNomUser());
//					System.out.println(vListeDesMessage.getMessage_rec().get(i).getMesage());
//					System.out.println("--------------------------------------");

				}
			}
		}

//		if (!vListeDesMessage.getMessage_send().isEmpty()) {
//			for (int i = 0; i < vListeDesMessage.getMessage_send().size(); i++) {
//				System.out.println(vListeUtilisateurs_send.get(i).getNomUser());
//			}
//		}

		pModelAndView.getModelMap().addAttribute("message_send", vListeDesMessage.getMessage_send());
		pModelAndView.getModelMap().addAttribute("user_send", vListeUtilisateurs_send);

		pModelAndView.getModelMap().addAttribute("message_rec", vListeDesMessage.getMessage_rec());
		pModelAndView.getModelMap().addAttribute("user_rec", vListeUtilisateurs_rec);

		pModelAndView.setViewName("messagerieDemmande");
		return pModelAndView;
	}

	@RequestMapping(value = "**/EnvoyerMessage", method = RequestMethod.GET)
	public ModelAndView EnvoyerMessageRun(HttpServletRequest request,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("MessageForm") MessageForm MessageForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");

		if (mMessageService.ajouterMessage(MessageForm.getIdDemmande(), user.getIdUser(), MessageForm.getText())) {
			System.out.println("controlleur " + MessageForm.getIdDemmande());

			vRedirectMap.put("MessageEnvoy�", "Le message a �t� envoy� avec succ�s");
			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:MesCommandes");
			return pModelAndView;

		} else {

			vRedirectMap.put("ErreurAjoutCommande",
					"une erreur de serveur s'est produite. votre trajet n'a pas �t� enregistr� ");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:AjouterCommandeDashboard");
			return pModelAndView;

		}

	}

	@RequestMapping(value = "**/EnvoyerMessagePerso", method = RequestMethod.GET)
	public ModelAndView EnvoyerMessagePersoRun(HttpServletRequest request, @RequestParam("idUser") int idUser,
			@ModelAttribute("rechercheTrajetForm") RechercheTrajetForm pRechercheTrajetForm,
			@ModelAttribute("MessageForm") MessageForm MessageForm,
			RedirectAttributesModelMap pRedirectAttributesModelMap, ModelAndView pModelAndView) {

		Map<String, Object> vRedirectMap = new HashMap<String, Object>();
		UtilisateurEntity user = (UtilisateurEntity) request.getSession().getAttribute("utilisateurSession");

		if (mMessageService.ajouterMessagePerso(idUser, user.getIdUser(), MessageForm.getText())) {

			vRedirectMap.put("MessageEnvoy�", "Le message a �t� envoy� avec succ�s");
			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:MesCommandes");
			return pModelAndView;

		} else {

			vRedirectMap.put("ErreurAjoutCommande",
					"une erreur de serveur s'est produite. votre trajet n'a pas �t� enregistr� ");

			pRedirectAttributesModelMap.addFlashAttribute("redirectMapAttribut", vRedirectMap);
			pModelAndView.setViewName("redirect:AjouterCommandeDashboard");
			return pModelAndView;

		}

	}

}
