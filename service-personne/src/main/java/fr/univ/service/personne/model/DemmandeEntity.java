package fr.univ.service.personne.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "demmande")
public class DemmandeEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_demmande")
	private int idDemmande;
	
	@Column(name = "id_commande")
	private int idCommande;

	
	@Column(name = "id_demmandeur")
	private int idUser;

	@Column(name = "etat")
	private String etat;

	@Column(name = "mdp")
	private String mdp;
	
	public int getIdDemmande() {
		return idDemmande;
	}

	public void setIdDemmande(int idDemmande) {
		this.idDemmande = idDemmande;
	}

	public int getIdCommande() {
		return idCommande;
	}

	public void setIdCommande(int idCommande) {
		this.idCommande = idCommande;
	}

	public int getIdUser() {
		return idUser;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public String getEtat() {
		return etat;
	}

	public void setEtat(String etat) {
		this.etat = etat;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	

	
}